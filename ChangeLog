2006-01-30  Matt Jones  <matt@mhjones.org>

	* AUTHORS: Changed email address
	* MAINTAINERS: Added maint file
	* TODO: Added todo list before 0.1 release

2006-01-07  Matt Jones  <mattjones@berkeley.edu>

	* src/nautilus-extension/Makefile.am: compensate for bad header file includes
	* src/nautilus-extension/nautilus-nrevision.c: (import_folder_cb): don't use import notification (for now)
	* src/rcs-utils.h: modify callback function format to include user data
	* src/svn-vfs/nr-import.c: (nr_import_cb), (import_folder): create notification window during imports - doesn't quite work yet, though
	* src/svn-vfs/svn-import.c: (nr_import): handle user data in import cb
	* src/svn-vfs/svn-import.h: handle user data in import cb
	* src/svn-vfs/svn-interface.c: (svn_import_cb), (svn_import): provide import status callback
	* src/svn-vfs/svn-interface.h: handle user data in import cb

2005-11-29  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.h: add import callback and actions enum
	* src/svn-vfs/svn-import.c: (nr_import_cb), (nr_import): add and use
	a default callback for import notification
	* src/svn-vfs/svn-interface.c: (svn_action_to_nr_action),
	(svn_import_cb), (svn_import): create svn->nr import callback glue;
	create and use a svn import notification callback
	* src/svn-vfs/svn-interface.h: change svn_import prototype (add callback)

2005-10-31  Matt Jones  <mattjones@berkeley.edu>

	* src/nautilus-revisioning.schema.in: add rm-directories key
	* src/daemon/daemon.c: (monitor_indiv_dir), (unmonitor_indiv_dir),
	(nr_inotify_recursive_monitor), (nr_inotify_recursive_unmonitor),
	(nr_inotify_read), (nr_check_queue), (nr_inotify_monitor_dir),
	(nr_inotify_unmonitor_dir), (nr_inotify_hash_remove),
	(nr_inotify_end_monitors), (gconf_add_monitor_lists),
	(gconf_rm_test), (gconf_rm_monitor_lists), (gconf_changed_cb),
	(main): Move to using a hash instead of a list of monitors, add functions to stop monitoring directories
	* src/nautilus-extension/nautilus-nrevision.c: (remove_folder_cb),
	(nautilus_nr_get_file_items): add "Stop Monitoring History" choice
	* src/rcs-utils.c: (rcs_gconf_directories): return both the currently monitored and the add list
	* src/rcs-utils.h: add the rm directory key path
	* src/svn-vfs/svn-import.c: (add_folder_to_gconf_rm), (nr_import),
	(nr_un_import): add functions to remove a folder from gconf
	* src/svn-vfs/svn-import.h: add prototype for nr_un_import

2005-10-30  Matt Jones  <mattjones@berkeley.edu>

	* configure.ac: bump version to 0.0.5

2005-10-30  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (gconf_ended_cb),
	(gconf_update_monitor_lists), (gconf_changed_cb), (main): daemon monitors newly added directories as they're added, directories can be added while daemon isn't running (although no sync)
	* src/nautilus-revisioning.schema.in: add new-directories key
	* src/rcs-utils.h: add some more GCONF path definitions
	* src/svn-vfs/svn-import.c: (add_folder_to_gconf): add to new-directories key and let daemon sync new-directories and directories

2005-10-29  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (main): make daemon use gconf to pickup folders
	* src/nautilus-extension/nautilus-nrevision.c: (import_folder_cb),
	(nautilus_nr_get_file_items): don't show the menu item if the folder is already monitored
	* src/svn-vfs/svn-import.c: (nr_import_exists): fix import_exists function

2005-10-24  Matt Jones  <mattjones@berkeley.edu>

	* src/nautilus-extension/Makefile.am: fix automake stuff
	* src/nautilus-extension/nautilus-nrevision.c: (import_folder_cb),
	(nautilus_nr_get_file_items), (nautilus_nr_register_type): correct nautilus extension code
	* src/svn-vfs/Makefile.am: split apart import
	* src/svn-vfs/import-test.c: remove old import cruft
	* src/svn-vfs/nr-import.c: (usage), (import_folder), (main): put main import loop in separate file
	* src/svn-vfs/svn-import.c: (add_folder_to_gconf), (nr_import),
	(nr_import_exists): library functions for imports
	* src/svn-vfs/svn-import.h: header for imports

2005-10-24  Matt Jones  <mattjones@berkeley.edu>

	* configure.ac: bump version number to reflect import changes, add extension makefile
	* src/Makefile.am: add extension directory
	* src/nautilus-extension/Makefile.am: makefile for nautilus extension
	* src/nautilus-extension/nautilus-nrevision.c: (import_folder_cb),
	(nautilus_nr_get_file_items),
	(nautilus_nr_menu_provider_iface_init),
	(nautilus_nr_instance_init), (nautilus_nr_class_init),
	(nautilus_nr_get_type), (nautilus_nr_register_type): nautilus extension for imports
	* src/nautilus-extension/nautilus-nrevision.h: ditto
	* src/nautilus-extension/nrevision-module.c: 
	(nautilus_module_initialize), (nautilus_module_shutdown),
	(nautilus_module_list_types): ditto

2005-10-20  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/import-test.c: (add_folder_to_gconf),
	(import_folder): add folders to gconf during import

2005-10-15  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/import-test.c: (usage), (import_folder), (main): gui for imports

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* patches/nautilus-patch.diff: this patch should work

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (nr_check_queue): fix movement of folders, handle move ins and outs correctly
	* src/daemon/event-handlers.c: 
	(nautilus_revisioning_handle_inotify_event): ditto
	* src/rcs-utils.c: (rcs_get_revision_shortname_for_display): whitespace
	* src/svn-vfs/svn-vfs.c: (do_get_file_info): revert incorrect change

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* patches/nautilus-patch.diff: update patch to head
	* src/rcs-utils.c: (rcs_get_revision_shortname_for_display): parse date instead of using ugly string

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* patches/nautilus-patch.diff: limit revision display to 5 revisions
	* src/svn-vfs/svn-interface.c: (svn_get_recent_revisions): func to return only n recent revisions
	* src/svn-vfs/svn-interface.h: proto for get_recent_revisions

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* configure.ac: bump version to 0.0.3
	* patches/nautilus-patch.diff: add tag interface
	* src/rcs-utils.h: add nautilus_rcs_do_tag prototype
	* src/svn-vfs/Makefile.am: add tag-test.c to build
	* src/svn-vfs/list-test.c: fix comments
	* src/svn-vfs/svn-interface.c: (nautilus_rcs_do_tag), (svn_tag): tagging fixes
	* src/svn-vfs/svn-vfs.c: (do_filecontrol): tagging fixes
	* src/svn-vfs/tag-test.c: (usage), (main): tag test program

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* BUILDING: forgot to mention that you need to install the gconf schema
	* configure.ac: bump version number to 0.0.2

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* BUILDING: update build instructions
	* src/nautilus-revisioning.schema.in: fix schemas

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* patches/nautilus-patch.diff: update patch
	* src/rcs-utils.c: (rcs_revision_get_uri): fix how the uri is generated
	* src/svn-vfs/Makefile.am: rename libsvn-gnomevfs to libsvn
	* src/svn-vfs/import-test.c: (main): small fix
	* src/svn-vfs/svn-interface.c: (svn_commit_txn): stop a weird bug from triggering
	* src/svn-vfs/svn-method.conf: libsvn-gnomevfs -> libsvn
	* src/svn-vfs/svn-prop.c: (svn_prop_import): set file info for parent dirs
	* src/svn-vfs/svn-utils.c: (svn_get_file_info_from_uri),
	(svn_get_revisioned_object_from_uri): handle non-managed file case, provide parent dir infos
	* src/svn-vfs/svn-vfs.c: (do_get_file_info): provide info for parent directories

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/list-test.c: (main): handle unmanaged directories
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri): handle unmanaged directories
	* src/svn-vfs/svn-vfs.c: (do_open_directory), (do_create),
	(do_make_directory), (do_move), (do_delete): gracefully handle unmanaged directories

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_get_revision_shortname_for_display),
	(rcs_revision_get_uri): remove some old stuff from shortname, add uri
	* src/rcs-utils.h: prototype
	* src/svn-vfs/list-test.c: (main): print uri
	* src/svn-vfs/svn-interface.c: (svn_get_revisions): return the correct revision number

2005-09-01  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_get_revision_shortname_for_display): return date rather than # by default
	* src/svn-vfs/Makefile.am: makefile.am fixes
	* src/svn-vfs/list-test.c: (main): use pretty-fied revision
	* src/svn-vfs/svn-interface.c: (svn_get_revisions): correctly generate revision list

2005-08-31  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (nr_check_queue): fix keyframing for moves
	* src/daemon/event-handlers.c:
	(nautilus_revisioning_handle_keyframe): use OPEN_READ instead of WRITE
	* src/svn-vfs/Makefile.am: add list-test to build, rename list-test and import-test
	* src/svn-vfs/import-test.c: (main): keyframe revision
	* src/svn-vfs/list-test.c: (usage), (main): tool to list significant revisions
	* src/svn-vfs/svn-interface.c: (svn_get_revisions): fix get_revisions

2005-08-31  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (nr_check_queue): add keyframe after events
	* src/daemon/event-handlers.c:
	(nautilus_revisioning_handle_keyframe): fix keyframing
	* src/svn-vfs/svn-interface.c: (svn_keyframe), (svn_tag): changes prototypes
	* src/svn-vfs/svn-interface.h: keyframing prototypes
	* src/svn-vfs/svn-prop.c: (svn_prop_import): keyframing fixes
	* src/svn-vfs/svn-vfs.c: (do_write), (do_filecontrol): keyframing fixes

2005-08-31  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/Makefile.am: include ../ in includes
	* src/daemon/event-handlers.c:
	(nautilus_revisioning_handle_keyframe): use RCSFileControlData
	* src/rcs-utils.h: move RCSFileControlDatat here
	* src/svn-vfs/svn-interface.h: move and rename SVNFileControlData
	* src/svn-vfs/svn-vfs.c: (do_filecontrol): use FileControlData correctly

2005-08-31  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (nr_check_queue): add keyframing
	* src/daemon/event-handlers.c:
	(nautilus_revisioning_handle_keyframe),
	(nautilus_revisioning_create_file),
	(nautilus_revisioning_update_file_content): keyframe, move create_file
	* src/rcs-utils.h: use a string for the date
	* src/svn-vfs/import-test.c: (main): status message
	* src/svn-vfs/svn-interface.c: (svn_keyframe), (svn_tag),
	(svn_import), (svn_get_revisions): keyframing & tagging changes, implementation of svn_get_revisions
	* src/svn-vfs/svn-interface.h: keyframing, prototypes
	* src/svn-vfs/svn-prop.c: (svn_prop_import): keyframe after propset
	* src/svn-vfs/svn-vfs.c: (do_filecontrol): keyframing changes

2005-08-30  Matt Jones  <mattjones@berkeley.edu>

	* patches/nautilus-patch.diff: update patch (display newest revision)
	* src/rcs-utils.c: (rcs_get_revision_shortname_for_display): render a revision to a string
	* src/rcs-utils.h: prototype for rcs_get_revision_for_display
	* src/svn-vfs/svn-interface.c: (svn_get_revisions): change arguments
	* src/svn-vfs/svn-interface.h: change prototype for svn_get_revisions
	* src/svn-vfs/svn-utils.h: change include

2005-08-30  Matt Jones  <mattjones@berkeley.edu>

	* patches/nautilus-patch.diff: button added as copy of location button

2005-08-30  Matt Jones  <mattjones@berkeley.edu>

	* patches/nautilus-patch.diff: initial work on adding a button
	* src/daemon/daemon.c: (nr_check_queue): comment out stuff

2005-08-30  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_keyframe), (svn_tag): add keyframe and tag operations
	* src/svn-vfs/svn-interface.h: add keyframe and tag struct and strings
	* src/svn-vfs/svn-vfs.c: (do_create), (do_filecontrol): typos, keyframe & tag support

2005-08-25  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (monitor_indiv_dir), (nr_check_queue): use gnome_vfs_append_file_path instead of _append_string (need to escape #)
	* src/daemon/event-handlers.c:
	(nautilus_revisioning_update_file_attrib),
	(nautilus_revisioning_update_file_content),
	(nautilus_revisioning_create_file),
	(nautilus_revisioning_delete_file),
	(nautilus_revisioning_handle_inotify_event): implement file handlers (ie full sync between fs->vfs)

2005-08-25  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (nr_check_queue): pass event handler full uri
	* src/daemon/event-handlers.c:
	(nautilus_revisioning_handle_inotify_event),
	(nautilus_revisioning_handle_move): handle move events correctly (daemon->vfs)
	* src/rcs-utils.c: (rcs_init): debug message
	* src/svn-vfs/svn-interface.c: (svn_init): debug message
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri),
	(svn_get_revision_from_uri): assume no number in URI == HEAD revision

2005-08-25  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/Makefile.am: build event-handlers.c
	* src/daemon/daemon.c: (event_equal), (print_queue),
	(nr_inotify_read), (nr_check_queue), (main): queue inotify events

2005-08-24  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (nr_inotify_event_to_string),
	(monitor_indiv_dir), (nr_inotify_recursive_monitor),
	(nr_inotify_fetch_queue), (nr_respond_to_inotify),
	(nr_inotify_monitor_dir), (nr_inotify_end_monitors), (main): rewrote to use inotify instead of fam
	* src/daemon/inotify-syscalls.h: inotify header
	* src/daemon/inotify.h: inotify header

2005-08-24  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (monitor_indiv_dir),
	(fam_recursive_monitor), (respond_to_fam), (fam_monitor_dir),
	(main): recursive monitoring now fully works

2005-08-22  Matt Jones  <mattjones@berkeley.edu>

	* src/daemon/daemon.c: (fam_monitor_dir), (fam_end_monitors),
	(monitor_indiv_dir), (main): recursive monitoring

2005-08-22  Matt Jones  <mattjones@berkeley.edu>

	* configure.ac: add src/daemon to the build
	* src/Makefile.am: add daemon to the build
	* src/daemon/Makefile.am: build nautilus-revisioning-daemon
	* src/daemon/daemon.c: (fam_event_to_string), (respond_to_fam),
	(main): very simple daemon, monitors for changes in /home/matt/Documents

2005-08-19  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-vfs.c: (do_get_file_info_from_handle),
	(do_make_directory), (do_move): do_move now works - meaning write support should be complete

2005-08-19  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_init), (svn_tag),
	(svn_start_txn), (svn_commit_txn), (svn_import),
	(svn_get_revisions), (svn_entry_to_robj), (svn_get_filelist),
	(svn_get_fileinfo), (svn_get_file), (svn_make_new_directory),
	(svn_cp), (svn_mv): removed \n from debug strings, various small fixes
	* src/svn-vfs/svn-interface.h: fixed prototypes, added begin_txn to header
	* src/svn-vfs/svn-utils.c: (svn_cleanup_path),
	(svn_repository_from_uri), (svn_get_rel_path_from_uri),
	(svn_get_revision_from_uri), (revision_is_youngest),
	(svn_get_revisioned_object_from_uri): changed debug flag to SVN_UTIL_DEBUG, changed call for revision_is_youngest to take a revision
	* src/svn-vfs/svn-utils.h: commented out debug flag, changed prototype for revision_is_youngest
	* src/svn-vfs/svn-vfs.c: (do_open), (do_close), (do_get_file_info),
	(do_make_directory), (do_delete): added do_delete code, fixed create code, various bugfixes

2005-08-18  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-vfs.c: (do_create), (do_close): move do_create's property importing to do_close for any file that's written to - thus solving the updating props problem

2005-08-18  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-vfs.c: (do_create), (do_close): add do_create, fixme in do_close

2005-08-18  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-utils.c: (svn_handle_new), (svn_handle_free): handle the txn added to SVNFileHandle
	* src/svn-vfs/svn-utils.h: add a svn_txn_t to SVNFileHandle
	* src/svn-vfs/svn-vfs.c: (do_open), (do_write), (do_close): implement write support - it actually is necessary, unlike what i thought before

2005-08-17  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_tag), (svn_start_txn),
	(svn_commit_txn), (svn_get_filelist), (svn_get_file),
	(svn_make_new_directory), (svn_cp), (svn_mv): implement directory creation, txn functions
	* src/svn-vfs/svn-prop.h: put add'l import function in header
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri),
	(revision_is_youngest), (svn_repo_new), (svn_repo_free): implement revision_is_youngest, which seems to work, but debug output doesn't appear correctly
	* src/svn-vfs/svn-utils.h: revision_is_youngest added to header
	* src/svn-vfs/svn-vfs.c: (do_open), (do_get_file_info_from_handle),
	(do_make_directory): hook up directory creation, enable write support flagging

2005-08-17  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-vfs.c: remove write function (not necessary)

2005-08-17  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-prop.c: (svn_prop_import): make sure to import the base directory
	* src/svn-vfs/svn-utils.c: (svn_cleanup_path),
	(svn_repository_from_uri): path disambiguation

2005-08-17  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.h: re-enable debug spew
	* src/svn-vfs/svn-prop.c: (svn_valid), (svn_get_prop_char),
	(svn_get_prop_guint), (svn_get_prop_size), (svn_get_prop_time),
	(svn_set_prop_char), (svn_set_prop_guint), (svn_set_prop_size),
	(svn_set_prop_time), (svn_prop_import_individual),
	(svn_prop_import): move prop set/get to SVN_PROP_DEBUG debug flag, fix recursive property imports so they actually work
	* src/svn-vfs/svn-prop.h: add and disable SVN_PROP_DEBUG debug flag
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri): small changes so we don't alloc a gchar*

2005-08-17  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_revision_free): don't free a const gchar*
	* src/svn-vfs/svn-interface.c: (svn_import): don't free const gchar*
	* src/svn-vfs/svn-interface.h: disable debug messages
	* src/svn-vfs/svn-prop.c: (svn_get_prop_char),
	(svn_get_prop_guint), (svn_get_prop_size), (svn_get_prop_time),
	(svn_set_prop_char), (svn_set_prop_guint), (svn_set_prop_size),
	(svn_set_prop_time), (svn_prop_import_individual): make sure debug messages are wrapped by ifdef's

2005-08-17  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_robj_free), (rcs_revision_new),
	(rcs_revision_free): memory fixes
	* src/rcs-utils.h: mem fixes
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri),
	(svn_get_revision_from_uri), (svn_repo_free), (svn_handle_free),
	(svn_get_revisioned_object_from_uri): mem fixes
	* src/svn-vfs/svn-vfs.c: (do_read), (do_get_file_info): mem fixes

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri),
	(svn_get_revision_from_uri), (svn_get_revisioned_object_from_uri): more memleak stuff
	* src/svn-vfs/svn-vfs.c: (do_read_directory), (do_close_directory),
	(do_open), (do_make_directory), (vfs_module_init): fixed some leaks

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_tag), (svn_commit),
	(svn_import), (svn_entry_to_robj), (svn_get_filelist): memory leakage fixing
	* src/svn-vfs/svn-interface.h: mv *new to svn-utils.h
	* src/svn-vfs/svn-utils.c: (svn_repo_new), (svn_handle_new),
	(svn_repo_free): move *new here, fix svn_repo_free to correctly handle different cases for repo->txn
	* src/svn-vfs/svn-utils.h: move svn_*_new here

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_init), (rcs_gconf_repository),
	(rcs_gconf_directories), (rcs_robj_new), (rcs_revision_new),
	(rcs_revision_free): fix leaks, add *_new methods
	* src/rcs-utils.h: add *_new methods

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_revision_free): temporarily disable freeing of unused properties
	* src/svn-vfs/svn-interface.h: move free definitions here
	* src/svn-vfs/svn-prop.c: (svn_valid), (svn_get_prop_char),
	(svn_get_prop_guint), (svn_get_prop_size), (svn_get_prop_time),
	(svn_set_prop_char), (svn_set_prop_guint), (svn_set_prop_size),
	(svn_set_prop_time): rename valid to svn_valid
	* src/svn-vfs/svn-utils.c: (svn_repo_free),
	(svn_get_revisioned_object_from_uri): defint svn_repo_free, mem fixes
	* src/svn-vfs/svn-utils.h: move out svn_handle_free def
	* src/svn-vfs/svn-vfs.c: (do_create), (do_open), (do_read),
	(do_write), (do_close), (do_get_file_info), (do_is_local),
	(do_make_directory): some initial write support (disabled for now)

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-utils.c: (svn_get_revisioned_object_from_uri): use the path, not the uri
	* src/svn-vfs/svn-vfs.c: (do_read): make reading work!

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_robj_free), (rcs_revision_free): add free functions
	* src/rcs-utils.h: add prototype for free functions; remove filhandle type
	* src/svn-vfs/Makefile.am: add svn-prop.[ch] to build
	* src/svn-vfs/svn-interface.c: some includes stuff
	* src/svn-vfs/svn-interface.h: includes stuff
	* src/svn-vfs/svn-prop.c: (valid), (svn_get_prop_char),
	(svn_get_prop_guint), (svn_get_prop_size), (svn_get_prop_time),
	(svn_get_single_fileinfo), (svn_set_prop_char),
	(svn_set_prop_guint), (svn_set_prop_size), (svn_set_prop_time),
	(svn_prop_import_individual), (svn_prop_import): move prop functions here
	* src/svn-vfs/svn-prop.h: move prop prototypes here
	* src/svn-vfs/svn-utils.c: (svn_get_rel_path_from_uri),
	(svn_get_revision_from_uri), (svn_handle_free),
	(svn_get_revisioned_object_from_uri): change uri scheme; split out prop functions; add filehandle type
	* src/svn-vfs/svn-utils.h: split out prop functions
	* src/svn-vfs/svn-vfs.c: (do_open), (do_read), (do_close): initial read support

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-utils.c: (svn_prop_import_individual): fixed a bug in getting file info

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-utils.c: (svn_get_prop_char),
	(svn_prop_import_individual): finally fixed this bug

2005-08-16  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-utils.c: (valid), (svn_get_prop_char),
	(svn_get_prop_guint), (svn_get_prop_size), (svn_get_prop_time),
	(svn_set_prop_char), (svn_set_prop_guint), (svn_set_prop_size),
	(svn_set_prop_time), (svn_prop_import_individual),
	(svn_prop_import): finally get the number stored to come back correctly in properties

2005-08-14  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_entry_to_robj),
	(svn_get_filelist): correctly build relative path

2005-08-14  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/Makefile.am: build import-test
	* src/svn-vfs/import-test.c: (usage), (main): import-test program
	* src/svn-vfs/svn-interface.c: (svn_log_cb), (svn_import),
	(svn_entry_to_robj): changes to fix some stuff
	* src/svn-vfs/svn-utils.c: (svn_set_prop_char),
	(svn_set_prop_guint), (svn_set_prop_size), (svn_set_prop_time),
	(svn_prop_import_individual), (svn_prop_import): fix import-related bugs

2005-08-11  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_import), (svn_entry_to_robj): modify import to import properties, change debug message
	* src/svn-vfs/svn-utils.c: (svn_get_single_fileinfo),
	(svn_set_prop_char), (svn_set_prop_guint), (svn_set_prop_size),
	(svn_set_prop_time), (svn_prop_import_indiv), (svn_prop_import): create property-setting functions
	* src/svn-vfs/svn-utils.h: add svn_prop_import to header

2005-08-09  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.h: add in disabled general rcs debug statement flag
	* src/svn-vfs/svn-interface.c: (svn_tag), (svn_entry_to_robj),
	(svn_get_filelist), (svn_get_fileinfo): changes to API
	* src/svn-vfs/svn-interface.h: changes to API
	* src/svn-vfs/svn-utils.c: (valid), (svn_get_prop_char),
	(svn_get_prop_guint), (svn_get_prop_size), (svn_get_prop_time),
	(svn_get_single_fileinfo), (svn_get_revisioned_object_from_uri): make fileinfo derive from subversion node properties

2005-08-09  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_gconf_repository), (rcs_gconf_directories): commented out debug stuff
	* src/svn-vfs/svn-interface.c: (svn_entry_to_robj),
	(svn_get_filelist): commented out debug stuff, fixed a couple of bugs
	* src/svn-vfs/svn-interface.h: turned off debug messages
	* src/svn-vfs/svn-utils.c: (svn_get_single_fileinfo): debug stuff
	* src/svn-vfs/svn-vfs.c: (do_read_directory): fixed a bug (wasn't copying the fileinfo correctly)

2005-08-07  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_entry_to_robj),
	(svn_get_filelist): small changes
	* src/svn-vfs/svn-vfs.c: (do_open_directory), (do_read_directory),
	(do_close_directory): fix handles so they actually work

2005-08-07  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_get_filelist): fixed issues with URI usage
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri): fixed uri usage

2005-08-07  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri): changed uri scheme

2005-08-07  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.c: (rcs_gconf_repository), (rcs_gconf_directories): debugging
	* src/svn-vfs/svn-interface.c: (svn_get_filelist): debugging
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri),
	(svn_get_revision_from_uri), (svn_get_single_fileinfo): debugging

2005-08-07  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/Makefile.am: link to apr & svn libs
	* src/svn-vfs/svn-vfs.c: (vfs_module_init), (vfs_module_shutdown): move vfs_module_* below method definition

2005-08-07  Matt Jones  <mattjones@berkeley.edu>

	* BUILDING:
	* src/svn-vfs/Makefile.am:

2005-08-07  Matt Jones  <mattjones@berkeley.edu>

	* BUILDING: instructions on building caveats
	* src/Makefile.am: add schema to install, build rcs-utils.c
	* src/nautilus-revisioning.schema.in: add gconf schema
	* src/rcs-utils.c: (rcs_gconf_repository), (rcs_gconf_directories): add utility functions for looking up gconf defaults
	* src/rcs-utils.h: add gconf functions to utilities, make a gchar* const
	* src/svn-vfs/svn-interface.c: (svn_get_filelist): use correct function
	* src/svn-vfs/svn-utils.c: (g_list_str_equal),
	(svn_repository_from_uri), (svn_get_rel_path_from_uri),
	(svn_get_revision_from_uri), (svn_get_single_fileinfo),
	(svn_get_revisioned_object_from_uri): write functions

2005-08-06  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-method.conf: adding file
	* src/svn-vfs/svn-utils.c: (svn_repository_from_uri),
	(svn_get_rel_path_from_uri), (svn_get_revision_from_uri),
	(svn_get_revisioned_object_from_uri): adding file
	* src/svn-vfs/svn-utils.h: adding file

2005-08-05  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_entry_to_robj),
	(svn_get_filelist), (svn_get_single_fileinfo), (svn_get_fileinfo): added get_sing_fileinfo, completed file info code, fixed a bug in svn_entry_to_robj
	* src/svn-vfs/svn-interface.h: changed some prototypes
	* src/svn-vfs/svn-vfs.c: (do_open_directory): updated with new api

2005-08-03  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_commit), (svn_get_revisions),
	(svn_entry_to_robj), (svn_get_filelist), (svn_get_fileinfo): svn_get_filelist implementation

2005-08-02  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_log_cb), (svn_import): use old implementation of import support (via svn_client)

2005-08-02  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_tag), (svn_commit): implement commit, change usage in svn_tag
	* src/svn-vfs/svn-interface.h: add txn field, change prototype for commit

2005-08-02  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_tag), (svn_commit),
	(svn_import), (svn_get_revisions): dropped svn_client usage
	* src/svn-vfs/svn-interface.h: renamed some properties

2005-08-02  Matt Jones  <mattjones@berkeley.edu>

	* configure.ac: add gconf requirement
	* src/rcs-utils.h: include shadow.h
	* src/shadow.c: (print_error), (print_link_error),
	(g_list_str_equal), (shadow_indiv_dir), (shadow_dir): reindent
	* src/svn-vfs/svn-interface.c: (g_list_str_equal),
	(clear_indiv_dir), (svn_generate_working_dir), (svn_import): implement basic import support
	* src/svn-vfs/svn-interface.h: add gconf key string

2005-07-31  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_commit): initial implementation of commit (w/o list of changes to commit)

2005-07-31  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_log_cb), (svn_tag): switch to using client library, implement basic (repos-wide) tagging support

2005-07-31  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_shutdown): use apr_terminate

2005-07-29  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.h: forgot to add init/shutdown functions t oheader
	* src/svn-vfs/svn-vfs.c: (vfs_module_init), (vfs_module_shutdown): actually use init / shutdown functions for svn interface

2005-07-29  Matt Jones  <mattjones@berkeley.edu>

	* configure.ac: add dependency on gnome-vfs-module-2.0
	* src/rcs-utils.h: fix indentation, add GnomeVFSFileInfo to RevisionedObjects, add RCSFileHandles
	* src/svn-vfs/Makefile.am: add build stuff for apr & svn deps
	* src/svn-vfs/svn-interface.c: (svn_init), (svn_shutdown),
	(svn_tag): implement initial tag svn API hookup, also init and shutdown functions
	* src/svn-vfs/svn-vfs.c: (do_open_directory), (do_read_directory),
	(do_close_directory), (do_open), (do_read), (do_close): initial implementation of vfs method (wont work - depends on working svn-interface.c)

2005-07-22  Matt Jones  <mattjones@berkeley.edu>

	* src/svn-vfs/svn-interface.c: (svn_tag), (svn_commit),
	(svn_import), (svn_get_revisions), (svn_get_filelist),
	(svn_get_fileinfo), (svn_get_fileinfo_recursive), (svn_get_file),
	(svn_cp), (svn_mv): changed spacing, renamed the various fileinfo and filelist functions
	* src/svn-vfs/svn-interface.h: messed with prototypes, added URI_TO_STRING macro
	* src/svn-vfs/svn-vfs.c: (vfs_module_init), (vfs_module_shutdown),
	(do_open_directory), (do_read_directory), (do_close_directory),
	(do_open), (do_read), (do_close), (do_is_local),
	(do_get_file_info): framework for a vfs module, not included in build yet

2005-07-19  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.h: License stuff
	* src/shadow-test.c: ditto
	* src/shadow.c: ditto
	* src/shadow.h: ditto
	* src/svn-vfs/svn-interface.c: ditto
	* src/svn-vfs/svn-interface.h: ditto

2005-07-19  Matt Jones  <mattjones@berkeley.edu>

	* src/rcs-utils.h: forgot to add this file before
	* src/svn-vfs/svn-interface.c: (svn_tag), (svn_commit),
	(svn_import), (svn_get_revisions), (svn_get_fileinfo),
	(svn_get_file), (svn_cp), (svn_mv): added debug spew to stubs
	* src/svn-vfs/svn-interface.h: enabled debug spew, got rid of svn_update function

2005-07-18  Matt Jones	<mattjones@berkeley.edu>

	* configure.ac: added src/svn-vfs/Makefile
	* src/Makefile.am: added src/svn-vfs as subdir
	* src/svn-vfs/Makefile.am: added build of libsvn-vfs library
	* src/svn-vfs/svn-interface.h: header for svn functions
	* src/svn-vfs/svn-interface.c: stub funtctions for svn operations
	
2005-07-13  Matt Jones  <mattjones@berkeley.edu>

	* src/Makefile.am: moved shadow functions into libversioning

2005-07-13  Matt Jones  <mattjones@berkeley.edu>

	* src/.cvsignore: made sure binary name was correct
	* src/Makefile.am: added shadow-test.c to build
	* src/shadow-test.c: (print_keys_pairs), (main): split out main and helper function into separate test program
	* src/shadow.c: (shadow_dir): ditto
	* src/shadow.h: moved externally-visible functions to header

