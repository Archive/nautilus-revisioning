/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* shadow-test.c: test of the shadowing code contained in shadow.h */

#include "shadow.h"

void print_keys_pairs (gpointer key,
		       gpointer value,
		       gpointer user_data)
{
  g_printf ("\"%s\" => \"%s\"\n", (char*)key, (char*)value);
}

int main (int argc, char **argv)
{
  GnomeVFSResult result;
  GHashTable *resolve;
  GList *reloc;

  if (!gnome_vfs_init ())
    g_error ("could not init gnomevfs!\n");

  reloc = NULL;
  reloc = g_list_append (reloc, ".svn");
  resolve = g_hash_table_new (g_str_hash, g_str_equal);

  result = shadow_dir ("/home/matt/shadow-temp", "/home/matt/gaim-sounds-link", reloc, resolve);

  g_printf ("Relocation database:\n");
  g_hash_table_foreach (resolve, (GHFunc)print_keys_pairs, NULL);

  if (result != GNOME_VFS_OK)
    print_error (result, "/home/matt/gaim-sounds-link");

  return 0;
}
