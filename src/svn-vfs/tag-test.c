/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* tag-test.c: tag a directory in the rcs repo 
   access */

#include "svn-interface.h"
#include "rcs-utils.h"
#include <glib.h>

int usage (char *prog)
{
	printf ("Usage: %s <path> <tag>\n\tpath: path to tag\n\ttag: tag to use\n", prog);
	
	return 0;
}

int main (int argc, char **argv)
{
	GnomeVFSHandle *handle;
	gchar *path, *tag;
	GnomeVFSURI *uri;
	RCSFileControlData *data;
	GnomeVFSResult result;
	
   	if (argc != 3)
		return usage (argv[0]);

	rcs_init ();
	svn_init ();
	if (!gnome_vfs_init ())
		return;

	path = argv[1];
	tag = argv[2];

	uri = gnome_vfs_uri_new (path);

	data = (RCSFileControlData*) g_malloc (sizeof (RCSFileControlData));
	data->uri = uri;
	data->tag_name = g_strdup (tag);
	gnome_vfs_uri_ref (uri);

	if (! gnome_vfs_init ())
		g_error ("couldn't init gnomevfs!\n");
	
	g_debug ("tagging %s with tag %s", gnome_vfs_uri_get_path (uri), tag);
	result = nautilus_rcs_do_tag (data);

	/*	g_debug ("=== opening ===");
	  result = gnome_vfs_open (&handle, path, GNOME_VFS_OPEN_READ);
	  if (result != GNOME_VFS_OK) g_error ("can't open uri %s: %s", path,
	  gnome_vfs_result_to_string (result));
	  
	  g_debug ("=== tagging ===");
	  result = gnome_vfs_file_control (handle, "tag", (gpointer)data);
	  if (result != GNOME_VFS_OK) g_error ("can't tag uri %s: %s", path,
	  gnome_vfs_result_to_string (result));
	  
	  g_debug ("=== closing ===");
	  result = gnome_vfs_close (handle);
	  if (result != GNOME_VFS_OK) g_error ("can't close uri %s: %s", path,
	  gnome_vfs_result_to_string (result));
	*/
	
	svn_shutdown ();
	gnome_vfs_shutdown ();

	return 0;
}

