/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* svn-vfs.c: GnomeVFS module for subversion */

#include <libgnomevfs/gnome-vfs-module.h>
#include <svn_pools.h>
#include "svn-interface.h"
#include "svn-utils.h"

typedef struct
{
	GList *files;
} FileListHandle;

static GnomeVFSResult
do_open_directory (GnomeVFSMethod           *method,
		   GnomeVFSMethodHandle    **method_handle,
		   GnomeVFSURI              *uri,
		   GnomeVFSFileInfoOptions   options,
		   GnomeVFSContext          *context)
{
	
	GList *files;
	SVNRepository *repo;
	gchar *rel_path;
	Revision *rev;
	FileListHandle *handle;

#ifdef SVN_VFS_DEBUG
	g_debug ("open directory (\"%s\")\n", URI_TO_STRING (uri));
#endif

	/* find the repository */

	repo = svn_repository_from_uri (uri);
	if (repo == NULL) return GNOME_VFS_ERROR_NOT_FOUND;
	rel_path = svn_get_rel_path_from_uri (repo, uri);
	rev = svn_get_revision_from_uri (repo, uri);

	/* get the list of objects in the current directory */
	files = svn_get_filelist (repo, rev, rel_path);

	/* set the last object to be NULL - so we can efficiently determine when to stop */
	files = g_list_append (files, NULL);
	
	/* return the list */
	handle = (FileListHandle*) g_malloc (sizeof (FileListHandle));
	handle->files = files;
	*method_handle = (GnomeVFSMethodHandle *) handle;
	
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read_directory (GnomeVFSMethod       *method,
		   GnomeVFSMethodHandle *method_handle,
		   GnomeVFSFileInfo     *file_info,
		   GnomeVFSContext      *context)
{
	FileListHandle *handle = (FileListHandle *) method_handle;
	GList *curr;

#ifdef SVN_VFS_DEBUG
	g_debug ("do_read_directory\n");
	if (!handle->files) g_error ("file list is null!\n");
#endif

	if (!handle->files->data)
			return GNOME_VFS_ERROR_EOF;
	
#ifdef SVN_VFS_DEBUG
	g_debug ("do_read_directory on file %s\n", 
		 ((RevisionedObject*)handle->files->data)->file_info->name);
#endif

	gnome_vfs_file_info_copy (file_info, ((RevisionedObject*)handle->files->data)->file_info);
	
	handle->files = g_list_next (handle->files);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close_directory (GnomeVFSMethod *method,
		    GnomeVFSMethodHandle *method_handle,
		    GnomeVFSContext *context)
{
	FileListHandle *handle = (FileListHandle *) method_handle;
	GList *curr;

#ifdef SVN_VFS_DEBUG
	g_debug ("closing directory\n");
#endif

	/* this code is only called if the directory is closed before being done read */
	if (handle)
		{
			curr = g_list_first (handle->files);
			rcs_revision_free (((RevisionedObject*)curr->data)->revision);

			for (;curr && curr->data;curr = g_list_next (curr))
				{
					gnome_vfs_uri_unref (((RevisionedObject*)curr->data)->uri);
					gnome_vfs_file_info_unref (((RevisionedObject*)curr->data)->file_info);
				}

			g_list_free (handle->files);
			g_free (handle);
		}

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_create (GnomeVFSMethod        *method,
	   GnomeVFSMethodHandle **method_handle,
	   GnomeVFSURI           *uri,
	   GnomeVFSOpenMode       mode,
	   gboolean               exclusive,
	   guint                  perm,
	   GnomeVFSContext       *context)
{
	Revision *revision;
	SVNRepository *repo;
	SVNFileHandle *handle;
	gchar *repo_path;
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_error_t *error;
	GnomeVFSResult result;

	handle = svn_handle_new ();
	handle->pool = svn_pool_create (NULL);

	/* Set up the repository access stuff */
	repo_path = rcs_gconf_repository ();
	error = svn_repos_open (&repos, repo_path, handle->pool);
	if (error) g_error ("Error opening repository for file access: %s\n", error->message);
	g_free (repo_path);
	fs = svn_repos_fs (repos);	

	/* Start the transaction */
	repo = svn_repository_from_uri (uri);
	if (repo == NULL) return GNOME_VFS_ERROR_NOT_FOUND;
	revision = svn_get_revision_from_uri (repo, uri);
	error = svn_fs_begin_txn (&(handle->txn), fs, revision->number, handle->pool);
	if (error) g_error ("Error starting transaction for file write: %s\n", error->message);
	error = svn_fs_txn_root (&(handle->root), handle->txn, handle->pool);
	if (error) g_error ("Error acquiring fs root for file access: %s\n", error->message);
	svn_repo_free (repo);

	/* Create the file */
	error = svn_fs_make_file (handle->root, gnome_vfs_uri_get_path (uri), handle->pool);
	if (error) g_error ("Error creating file: %s\n", error->message);
	
	/* Set up the write stream */
	error = svn_fs_apply_text (&(handle->contents), handle->root,
				   gnome_vfs_uri_get_path (uri), NULL, handle->pool);
	if (error) g_error ("Error acquiring file stream for file access: %s\n", error->message);

	/* create the revisioned object */
	handle->object = rcs_robj_new ();
	handle->object->uri = gnome_vfs_uri_dup (uri);
	handle->object->revision = revision;
	handle->object->file_info = gnome_vfs_file_info_new ();
	result = gnome_vfs_get_file_info (gnome_vfs_uri_get_path (uri), handle->object->file_info, 
					  GNOME_VFS_FILE_INFO_GET_MIME_TYPE 
					  | GNOME_VFS_FILE_INFO_GET_ACCESS_RIGHTS);
	if (result != GNOME_VFS_OK) g_error ("couldn't retrieve new file's file-info for robj\n");

	*method_handle = (GnomeVFSMethodHandle *) handle;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_open (GnomeVFSMethod        *method,
	 GnomeVFSMethodHandle **method_handle,
	 GnomeVFSURI           *uri,
	 GnomeVFSOpenMode       mode,
	 GnomeVFSContext       *context)
{
	RevisionedObject *file;
	SVNFileHandle *handle;
	gchar *repo_path;
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_error_t *error;
	
	file = svn_get_revisioned_object_from_uri (uri);

#ifdef SVN_VFS_DEBUG
	g_debug ("opening file %s\n", file->file_info->name);
#endif

	if (!file) return GNOME_VFS_ERROR_NOT_FOUND;

	if (file && 
	    (file->file_info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_TYPE) &&
	    (file->file_info->type & GNOME_VFS_FILE_TYPE_DIRECTORY))
		{
			rcs_robj_free (file);
			return GNOME_VFS_ERROR_IS_DIRECTORY;
		}

	/* no random mode... yet */
	if (mode & GNOME_VFS_OPEN_RANDOM)
		{
			rcs_robj_free (file);
			return GNOME_VFS_ERROR_INVALID_OPEN_MODE;
		}
	
	/* we can only write to the newest revision */
	else if (mode & GNOME_VFS_OPEN_WRITE && ! revision_is_youngest (file->revision))
		{
			rcs_robj_free (file);
			return GNOME_VFS_ERROR_READ_ONLY;
		}
	
	handle = svn_handle_new ();
	handle->object = file;
	handle->pool = svn_pool_create (NULL);
	
	repo_path = rcs_gconf_repository ();
	error = svn_repos_open (&repos, repo_path, handle->pool);
	if (error) g_error ("Error opening repository for file access: %s\n", error->message);
	g_free (repo_path);
	fs = svn_repos_fs (repos);

	if (mode & GNOME_VFS_OPEN_READ)
		{
			error = svn_fs_revision_root (&(handle->root), fs, file->revision->number, handle->pool);
			if (error) g_error ("Error acquiring fs root for file access: %s\n", error->message);
			
			error = svn_fs_file_contents (&(handle->contents), handle->root, 
						      gnome_vfs_uri_get_path (uri), handle->pool);
			if (error) g_error ("Error acquiring file stream for file access: %s\n", error->message);
		}
	else /* mode == GNOME_VFS_WRITE */
		{
			error = svn_fs_begin_txn (&(handle->txn), fs, file->revision->number, handle->pool);
			if (error) g_error ("Error starting transaction for file write: %s\n", error->message);
			error = svn_fs_txn_root (&(handle->root), handle->txn, handle->pool);
			if (error) g_error ("Error acquiring fs root for file access: %s\n", error->message);
			
			error = svn_fs_apply_text (&(handle->contents), handle->root,
						   gnome_vfs_uri_get_path (uri), NULL, handle->pool);
		}

	*method_handle = (GnomeVFSMethodHandle *) handle;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_read (GnomeVFSMethod       *method,
	 GnomeVFSMethodHandle *method_handle,
	 gpointer              buffer,
	 GnomeVFSFileSize      bytes,
	 GnomeVFSFileSize     *bytes_read,
	 GnomeVFSContext      *context)
{
	SVNFileHandle *handle = (SVNFileHandle *) method_handle;
	svn_error_t *error;
	apr_size_t len;
	
#ifdef SVN_VFS_DEBUG
	g_debug ("reading %Lu bytes from file %s\n", bytes, handle->object->file_info->name);
#endif

	len = (size_t) bytes;

	error = svn_stream_read (handle->contents, (char*)buffer, &len);

	*bytes_read = (unsigned long long) len;

	if (len == 0)
		return GNOME_VFS_ERROR_EOF;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_write (GnomeVFSMethod       *method,
	  GnomeVFSMethodHandle *method_handle,
	  gconstpointer         buffer,
	  GnomeVFSFileSize      bytes,
	  GnomeVFSFileSize     *bytes_written,
	  GnomeVFSContext      *context)
{
	SVNFileHandle *handle = (SVNFileHandle *) method_handle;
	svn_error_t *error;
	apr_size_t len;
	
#ifdef SVN_VFS_DEBUG
	g_debug ("writing %Lu bytes to file %s\n", bytes, handle->object->file_info->name);
#endif

	if (! svn_fs_is_txn_root (handle->root))
		g_error ("root does not associate with a txn!\n");

	len = (size_t) bytes;

	error = svn_stream_write (handle->contents, buffer, &len);
	if (error) g_error ("Error writing: %s\n", error->message);

	*bytes_written = (unsigned long long) len;

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_close (GnomeVFSMethod       *method,
	  GnomeVFSMethodHandle *method_handle,
	  GnomeVFSContext      *context)
{
	SVNFileHandle *handle = (SVNFileHandle *) method_handle;
	GnomeVFSResult result;
	svn_error_t *error;
	svn_revnum_t rev;
	const gchar *conflicts;

#ifdef SVN_VFS_DEBUG
	g_debug ("closing file %s\n", handle->object->file_info->name);
#endif

	error = svn_stream_close (handle->contents);
	if (error) g_error ("Error closing file stream: %s\n", error->message);

	if (svn_fs_is_txn_root (handle->root))
		{
			/* Update/Import file properties */
			result = svn_prop_import_individual (handle->root, 
							     gnome_vfs_uri_get_path (handle->object->uri), 
							     handle->pool);
			if (result != GNOME_VFS_OK) g_error ("couldn't import new file's props!\n");

			error = svn_fs_commit_txn (&conflicts, &rev, handle->txn, handle->pool);
			if (error) g_error ("Error committing transaction: %s\n", error->message);
#ifdef SVN_VFS_DEBUG
			else g_debug ("Commited file write with new revision %d\n", rev);
#endif
		}

	svn_handle_free (handle);
	
	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_get_file_info (GnomeVFSMethod          *method,
		  GnomeVFSURI             *uri,
		  GnomeVFSFileInfo        *file_info,
		  GnomeVFSFileInfoOptions  options,
		  GnomeVFSContext         *context)
{
	RevisionedObject *file;

#ifdef SVN_VFS_DEBUG
	g_debug ("getting file info for uri %s\n", URI_TO_STRING (uri));
#endif

	file = svn_get_revisioned_object_from_uri (uri);

	/* check for existence of file */
	if (file) 
		{
			gnome_vfs_file_info_copy (file_info, file->file_info);
			rcs_robj_free (file);
			return GNOME_VFS_OK;
		}
	else
		return GNOME_VFS_ERROR_NOT_FOUND;
		/*	gnome_vfs_file_info_copy (file_info, svn_get_file_info_from_uri (uri));*/
	
	/*	return GNOME_VFS_OK; */
}

static GnomeVFSResult
do_get_file_info_from_handle (GnomeVFSMethod          *method,
			      GnomeVFSMethodHandle    *method_handle,
			      GnomeVFSFileInfo        *file_info,
			      GnomeVFSFileInfoOptions  options,
			      GnomeVFSContext         *context)
{
	SVNFileHandle *handle = (SVNFileHandle *) method_handle;

#ifdef SVN_VFS_DEBUG
	g_debug ("getting file info from handle\n");
#endif
	
	gnome_vfs_file_info_copy (file_info, handle->object->file_info);
	
	return GNOME_VFS_OK;
}

static gboolean
do_is_local (GnomeVFSMethod    *method,
	     const GnomeVFSURI *uri)
{
#ifdef SVN_VFS_DEBUG
	g_debug ("file is not local\n");
#endif
	/* do we want nautilus to thumbnail? this is debatable */
	return FALSE;
}

static GnomeVFSResult
do_make_directory (GnomeVFSMethod  *method,
		   GnomeVFSURI     *uri,
		   guint            perm,
		   GnomeVFSContext *context)
{
	RevisionedObject *file;
	SVNRepository *repo;
	GnomeVFSResult result;
	GnomeVFSURI *parent;

#ifdef SVN_VFS_DEBUG
	g_debug ("creating new directory at uri %s\n", URI_TO_STRING (uri));
#endif

	parent = gnome_vfs_uri_get_parent (uri);
	file = svn_get_revisioned_object_from_uri (parent);
	repo = svn_repository_from_uri (uri);
	if (repo == NULL) return GNOME_VFS_ERROR_NOT_FOUND;
	gnome_vfs_uri_unref (parent);

	if (! revision_is_youngest (file->revision))
	  return GNOME_VFS_ERROR_READ_ONLY; 

	result = svn_make_new_directory (repo, uri);

	rcs_robj_free (file);
	svn_repo_free (repo);

	return result;
}

static GnomeVFSResult
do_move (GnomeVFSMethod  *method,
	 GnomeVFSURI     *old_uri,
	 GnomeVFSURI     *new_uri,
	 gboolean         force_replace,
	 GnomeVFSContext *context)
{
	SVNRepository *repo;
	RevisionedObject *robj_old, *robj_new;
	GnomeVFSResult result;
	svn_fs_t *fs;
	svn_fs_root_t *root, *readroot;
	svn_error_t *error;

#ifdef SVN_VFS_DEBUG
	g_debug ("moving uri %s to %s\n", URI_TO_STRING (old_uri), URI_TO_STRING (new_uri));
#endif

	repo = svn_repository_from_uri (old_uri);
	if (repo == NULL) return GNOME_VFS_ERROR_NOT_FOUND;
	robj_old = svn_get_revisioned_object_from_uri (old_uri);

	if (!robj_old) 
		{
			svn_repo_free (repo);
			return GNOME_VFS_ERROR_NOT_FOUND;
		}

	if (! revision_is_youngest (robj_old->revision))
		{
			svn_repo_free (repo);
			rcs_robj_free (robj_old);
			return GNOME_VFS_ERROR_READ_ONLY; 
		}

	robj_new = svn_get_revisioned_object_from_uri (new_uri);

	if (robj_new && !force_replace)
		{
			rcs_robj_free (robj_old);
			rcs_robj_free (robj_new);
			svn_repo_free (repo);
			return GNOME_VFS_ERROR_FILE_EXISTS;
		}
	
	result = svn_start_txn (repo);
	if (result != GNOME_VFS_OK) g_error ("couldn't start txn!\n");
	error = svn_fs_txn_root (&root, repo->txn, repo->pool);
	if (error) g_error ("Error getting txn root\n");

	if (robj_new)
		{
			error = svn_fs_delete (root, gnome_vfs_uri_get_path (new_uri), repo->pool);
			if (error) g_error ("couldn't delete object %s: %s\n", gnome_vfs_uri_get_path (new_uri),
					    error->message);
		}

	/* get a root for the old file */
	fs = svn_fs_root_fs (root);
	error = svn_fs_revision_root (&readroot, fs, robj_old->revision->number, repo->pool);	
	if (error) g_error ("Error opening read-only repo root: %s\n", error->message);

	/* Copy the old file to the new file */
	error = svn_fs_copy (readroot, gnome_vfs_uri_get_path (old_uri), 
			     root, gnome_vfs_uri_get_path (new_uri), repo->pool);
	if (error) g_error ("couldn't copy object: %s\n", error->message);

	/* Delete the old file */
	error = svn_fs_delete (root, gnome_vfs_uri_get_path (old_uri), repo->pool);
	if (error) g_error ("couldn't delete object %s: %s\n", gnome_vfs_uri_get_path (old_uri),
			    error->message);

	/* Update/Import file properties */
	result = svn_prop_import_individual (root, 
					     gnome_vfs_uri_get_path (new_uri), 
					     repo->pool);
	if (result != GNOME_VFS_OK) g_error ("couldn't import new file's props!\n");
	
	/* Commit the transaction */
	result = svn_commit_txn (repo);
	if (result != GNOME_VFS_OK) g_error ("couldn't commit txn!\n");

	rcs_robj_free (robj_old);
	if (robj_new) rcs_robj_free (robj_new);
	svn_repo_free (repo);

	return GNOME_VFS_OK;	
}

static GnomeVFSResult
do_delete (GnomeVFSMethod  *method,
	   GnomeVFSURI     *uri,
	   GnomeVFSContext *context)
{
	SVNRepository *repo;
	RevisionedObject *robj;
	GnomeVFSResult result;
	svn_fs_root_t *root;
	svn_error_t *error;

	repo = svn_repository_from_uri (uri);
	if (repo == NULL) return GNOME_VFS_ERROR_NOT_FOUND;
	robj = svn_get_revisioned_object_from_uri (uri);

	if (!robj) 
		{
			svn_repo_free (repo);
			return GNOME_VFS_ERROR_NOT_FOUND;
		}

	if (! revision_is_youngest (robj->revision))
		{
			svn_repo_free (repo);
			rcs_robj_free (robj);
			return GNOME_VFS_ERROR_READ_ONLY; 
		}

	rcs_robj_free (robj);

	result = svn_start_txn (repo);
	if (result != GNOME_VFS_OK) g_error ("couldn't start txn!\n");
	error = svn_fs_txn_root (&root, repo->txn, repo->pool);
	if (error) g_error ("Error getting txn root\n");
	error = svn_fs_delete (root, gnome_vfs_uri_get_path (uri), repo->pool);
	if (error) g_error ("couldn't delete object %s: %s\n", gnome_vfs_uri_get_path (uri),
			    error->message);
	result = svn_commit_txn (repo);
	if (result != GNOME_VFS_OK) g_error ("couldn't commit txn!\n");

	svn_repo_free (repo);

	return GNOME_VFS_OK;
}

static GnomeVFSResult
do_filecontrol (GnomeVFSMethod       *method,
		GnomeVFSMethodHandle *method_handle,
 		const char           *operation,
 		gpointer              operation_data,
 		GnomeVFSContext      *context)
{
 	RCSFileControlData *data;
 	GnomeVFSResult result;
	
#ifdef SVN_VFS_DEBUG
	g_debug ("do_filecontrol");
#endif

 	data = (RCSFileControlData *) operation_data;
 	
	g_printf ("uri: %s; op: %s\n", gnome_vfs_uri_get_path (data->uri), operation);

 	if (g_str_equal (operation, "tag"))
		{
			if ((result = svn_keyframe (gnome_vfs_uri_get_parent (data->uri))) == GNOME_VFS_OK)
				result = svn_tag (data->tag_name, gnome_vfs_uri_get_parent (data->uri));
		}
 	else /*if (g_str_equal (operation, "keyframe")) */
		result = svn_keyframe (gnome_vfs_uri_get_parent (data->uri));
	/* 	else 
	   result = GNOME_VFS_ERROR_NOT_SUPPORTED; */
 
	g_debug ("result: %s", gnome_vfs_result_to_string (result));

 	return result;
}

static GnomeVFSMethod method = {
	sizeof (GnomeVFSMethod),
	
	do_open,
	do_create,
	do_close,
	do_read,
	do_write,
	NULL,
	NULL,
	NULL,
	do_open_directory,
	do_close_directory,
	do_read_directory,
	do_get_file_info,
	do_get_file_info_from_handle,
	do_is_local,
	do_make_directory,
       	do_delete,
	do_move,
	do_delete,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	do_filecontrol
};

GnomeVFSMethod *
vfs_module_init (const char *method_name, const char *args)
{
#ifdef SVN_VFS_DEBUG
	g_debug ("init called for method %s\n", method_name);
#endif

	if (strcmp (method_name, "svn") == 0) {
		/* init code */
		
#ifdef SVN_VFS_DEBUG
		g_debug ("init method called\n");
#endif

		rcs_init ();
		svn_init ();

		return &method;
	}
	
	return NULL;
}

void
vfs_module_shutdown (GnomeVFSMethod *method)
{
	/* shutdown stuff */
#ifdef SVN_VFS_DEBUG
	g_debug ("shutdown method called\n");
#endif

	svn_shutdown ();
}
