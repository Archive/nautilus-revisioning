/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* svn-utils.h: Utilities for working with svn types */

#ifndef _SVN_UTILS_H_
#define _SVN_UTILS_H_

#include <libgnomevfs/gnome-vfs.h>
#include <glib.h>
#include <svn_repos.h>

#include "rcs-utils.h"

/* #define SVN_UTIL_DEBUG */

typedef struct
{
	GnomeVFSURI *directory;
	GnomeVFSURI *repository;
	svn_fs_txn_t *txn;
	apr_pool_t *pool;
} SVNRepository;

typedef struct
{
	RevisionedObject *object;
	svn_fs_root_t *root;
	svn_fs_txn_t *txn;
	svn_stream_t *contents;
	apr_pool_t *pool;
} SVNFileHandle;

SVNRepository* svn_repository_from_uri (GnomeVFSURI *uri);

gchar* svn_get_rel_path_from_uri (SVNRepository *repo, GnomeVFSURI *uri);
Revision* svn_get_revision_from_uri (SVNRepository *repo, GnomeVFSURI *uri);
RevisionedObject* svn_get_revisioned_object_from_uri (GnomeVFSURI *uri);

gboolean revision_is_youngest (Revision *file);

SVNRepository* svn_repo_new ();
SVNFileHandle* svn_handle_new ();

/* Memory management */
void svn_repo_free (SVNRepository *repo);
void svn_handle_free (SVNFileHandle *handle);

#endif
