/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* svn-interface.h: interface to command line subversion functionality
   cli is stand-in for libsvn, as libsvn is somewhat complex */

#ifndef _SVN_INTERFACE_H_
#define _SVN_INTERFACE_H_

#include <svn_repos.h>
#include "rcs-utils.h"
#include "svn-utils.h"

#define SVN_VFS_DEBUG
#define URI_TO_STRING(x) (gnome_vfs_uri_to_string(x,GNOME_VFS_URI_HIDE_NONE))

#define SVN_INTERFACE_GCONF_WORKING_DIR "test"

#define SVN_INTERFACE_AUTHOR "nautilus-revisioning"

/* property names */
#define SVN_PROPNAME_TAG         "tag"
#define SVN_PROPNAME_KEYFRAME    "keyframe"
#define SVN_PROPNAME_DATE        "keyframe-date"
#define SVN_PROPNAME_NAME        "name"
#define SVN_PROPNAME_VALIDFIELDS "validfields"
#define SVN_PROPNAME_TYPE        "type"
#define SVN_PROPNAME_PERMS       "perms"
#define SVN_PROPNAME_FLAGS       "flags"
#define SVN_PROPNAME_UID         "uid"
#define SVN_PROPNAME_GID         "gid"
#define SVN_PROPNAME_SIZE        "size"
#define SVN_PROPNAME_BLOCKS      "blocks"
#define SVN_PROPNAME_ATIME       "atime"
#define SVN_PROPNAME_MTIME       "mtime"
#define SVN_PROPNAME_CTIME       "ctime"
#define SVN_PROPNAME_SYMLINK     "symlink"
#define SVN_PROPNAME_MIME        "mime"

/* strings for file control in vfs method */
#define SVN_VFS_FILE_CONTROL_TAG      "tag"
#define SVN_VFS_FILE_CONTROL_KEYFRAME "keyframe"

/* Initialization / Shutdown routines */
void svn_init ();
void svn_shutdown ();

/* Repository-wide operations */
GnomeVFSResult svn_tag (const gchar *name, GnomeVFSURI *uri);
GnomeVFSResult svn_keyframe (GnomeVFSURI *uri);
GnomeVFSResult svn_start_txn (SVNRepository *repo);
GnomeVFSResult svn_commit_txn (SVNRepository *repo);
GnomeVFSResult svn_import (GnomeVFSURI *src_uri, 
			   GnomeVFSURI *repo_uri, 
			   SVNRepository **repo, 
			   nr_notify_cb *notify,
			   void *baton);

/* Get info about the repository */
GList* svn_get_revisions (SVNRepository *repo, GnomeVFSURI *uri);
GList* svn_get_recent_revisions (SVNRepository *repo, GnomeVFSURI *uri, gint number);
GList* svn_get_filelist (SVNRepository *repo, Revision *revision, gchar *rel_path);
GnomeVFSFileInfo* svn_get_single_fileinfo (svn_fs_root_t *fs, gchar *rel_path, apr_pool_t *pool);
GList* svn_get_fileinfo (SVNRepository *repo, Revision *revision, GList *items, gchar *rel_path);

/* Individual operations on files or directories*/
GnomeVFSURI* svn_get_file (SVNRepository *repo, RevisionedObject *obj);
GnomeVFSResult svn_cp (SVNRepository *repo, RevisionedObject *obj, GnomeVFSURI *new_uri);
GnomeVFSResult svn_mv (SVNRepository *repo, RevisionedObject *obj, GnomeVFSURI *new_uri);

#endif /* _SVN_INTERFACE_H_ */
