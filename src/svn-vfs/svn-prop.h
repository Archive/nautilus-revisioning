#ifndef _SVN_PROP_H_
#define _SVN_PROP_H_

/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

#include "svn-utils.h"
#include "svn-interface.h"

/* svn-prop.h: Utilities for working with svn properties */

/* #define SVN_PROP_DEBUG */

GnomeVFSResult svn_prop_import (GnomeVFSURI *src, GnomeVFSURI *repo);
GnomeVFSResult svn_prop_import_individual (svn_fs_root_t *fs, const gchar *path, apr_pool_t *pool);
#endif
