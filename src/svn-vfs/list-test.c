/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* list-test.c: list revisions for a directory */

#include "svn-interface.h"
#include "rcs-utils.h"
#include <glib.h>

int usage (char *prog)
{
	printf ("Usage: %s <path>\n\tpath: path to import into repo\n", prog);
	
	return 0;
}

int main (int argc, char **argv)
{
	char *path, *repo;
	GnomeVFSURI *dir, *repository;
	GList *list;
	SVNRepository *repos;
	
   	if (argc != 2)
		return usage (argv[0]);

	rcs_init ();
	svn_init ();

	path = argv[1];
	repo = rcs_gconf_repository ();
	
	if (! gnome_vfs_init ())
		g_error ("couldn't init gnomevfs!\n");
	
	dir = gnome_vfs_uri_new (path);
	repository = gnome_vfs_uri_new (repo);
	
	repos = svn_repository_from_uri (dir);
	if (repos == NULL) g_error ("directory not managed");
	g_debug ("about to get revisions");
	list = svn_get_revisions (repos, dir);

	while (list)
		{
			Revision *rev = list->data;
			gchar *temp;

			temp = rcs_get_revision_shortname_for_display (rev);
			g_debug ("rev: %s", temp);
			/*			g_free (temp);*/

			temp = URI_TO_STRING (rcs_revision_get_uri (dir, rev));
			g_debug ("rev URI: %s", temp);
			g_free (temp);

			list = g_list_next (list);
		}
	
	svn_shutdown ();
	gnome_vfs_shutdown ();

	return 0;
}

