/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* svn-import.c: functions to import a directory into the rcs repo 
   & set it up for gnomevfs access */

#include "svn-import.h"
#include "svn-interface.h"
#include "rcs-utils.h"
#include <glib.h>
#include <gconf/gconf-client.h>

void
add_folder_to_gconf (const gchar *uri)
{
	GConfClient *client;
	GError *err = NULL;
	GSList *list;
	gboolean result;

	g_debug ("Adding URI %s to gconf", uri);

	client = gconf_client_get_default ();

	list = gconf_client_get_list (client,
				      RCS_GCONF_NEW_DIRECTORIES,
				      GCONF_VALUE_STRING,
				      &err);
	list = g_slist_append (list, g_strdup (uri));
	
	result = gconf_client_set_list (client, RCS_GCONF_NEW_DIRECTORIES,
					GCONF_VALUE_STRING, list,
					&err);
	if (!result)
		g_error ("Couldn't add new directory to managed dirs list");
}

void
add_folder_to_gconf_rm (const gchar *uri)
{
	GConfClient *client;
	GError *err = NULL;
	GSList *list;
	gboolean result;

	g_debug ("Removing URI %s from gconf", uri);

	client = gconf_client_get_default ();

	list = gconf_client_get_list (client,
				      RCS_GCONF_RM_DIRECTORIES,
				      GCONF_VALUE_STRING,
				      &err);
	list = g_slist_append (list, g_strdup (uri));
	
	result = gconf_client_set_list (client, RCS_GCONF_RM_DIRECTORIES,
					GCONF_VALUE_STRING, list,
					&err);
	if (!result)
		g_error ("Couldn't remove old directory from managed dirs list");
}

static void
nr_import_cb (const gchar *path,
	      NRAction action)
{
	if (action == NR_ADDED)
		g_debug ("added %s", path);
	if (action == NR_DELTA)
		g_debug ("delta for %s", path);
}

void 
nr_import (const gchar *path, nr_notify_cb *cb, void *baton)
{
	const gchar *repo;
	GnomeVFSURI *dir, *repository;
	SVNRepository *repos;

	repo = rcs_gconf_repository ();
	
	dir = gnome_vfs_uri_new (path);
	repository = gnome_vfs_uri_new (repo);

	if (svn_import (dir, repository, &repos, (nr_notify_cb*)cb, baton) == GNOME_VFS_OK)
		g_printf ("Successfully imported directory %s\n", path);

	add_folder_to_gconf (path);
	
	dir = gnome_vfs_uri_new (path);
	if (svn_keyframe (dir) == GNOME_VFS_OK) {}
}

void 
nr_un_import (const gchar *path)
{
	const gchar *repo;
	GnomeVFSURI *dir, *repository;
	SVNRepository *repos;

	add_folder_to_gconf_rm (path);
}

gboolean
nr_import_exists (const gchar *path)
{
	GConfClient *client;
	GError *err = NULL;
	GSList *list;
	gboolean found = FALSE;

	client = gconf_client_get_default ();

	list = gconf_client_get_list (client,
				      RCS_GCONF_DIRECTORIES,
				      GCONF_VALUE_STRING,
				      &err);
	
	g_debug ("Checking if path \"%s\" is managed", path);

	for (; list; list = g_slist_next (list))
		{
			g_debug ("list->data: \"%s\"", list->data);
			if (g_str_equal (path, list->data))
				found = TRUE;
			g_free (list->data);
		}
	if (found) g_debug ("folder found");
	else g_debug ("folder not found");

	g_slist_free (list);
	return found;
}
