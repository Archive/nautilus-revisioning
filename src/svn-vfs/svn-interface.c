/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* svn-interface.c: Implementation of interface to subversion api */

#include "svn-interface.h"
#include "svn-prop.h"
#include "rcs-utils.h"
#include <svn_repos.h>
#include <svn_client.h>
#include <svn_pools.h>
#include <apr_time.h>

/* Initialization / Shutdown routines */

void svn_init ()
{
	g_debug ("svn_init called");
	if (apr_initialize () != APR_SUCCESS)
		g_error ("Wasn't able to initialize apr!");
}

void svn_shutdown ()
{
	apr_terminate ();
}

GnomeVFSResult nautilus_rcs_do_tag (RCSFileControlData *data)
{
	return svn_tag (data->tag_name, data->uri);
}

/* Repository-wide operations */
GnomeVFSResult svn_keyframe (GnomeVFSURI *uri)
{
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_revnum_t head;
	svn_string_t *tagname;
	svn_fs_txn_t *txn;
	svn_fs_root_t *root;
	SVNRepository *repo;

	apr_pool_t *pool;
	svn_error_t *error;

#ifdef SVN_VFS_DEBUG
	g_debug ("keyframing repo @ %s", gnome_vfs_uri_get_path (uri));
#endif
	
	repo = svn_repository_from_uri (uri);

	pool = svn_pool_create (NULL);

	/* Acquire the svn filesystem */
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), pool);
	if (error) g_error ("error setting keyframe: %s", error->message);
	fs = svn_repos_fs (repos);

	/* Get the revision for head */
	error = svn_fs_youngest_rev (&head, fs, pool);
	if (error) g_error ("error setting keyframe: %s", error->message);
	error = svn_fs_begin_txn (&txn, fs, head, pool);
	if (error) g_error ("error setting keyframe: %s", error->message);
	error = svn_fs_txn_root (&root, txn, pool);
	if (error) g_error ("error setting keyframe: %s", error->message);

	/* Create a string for the tag */
	tagname = svn_string_create (SVN_PROPNAME_KEYFRAME, pool);

	/* Name the most recent revision */
	error = svn_fs_change_node_prop (root, gnome_vfs_uri_get_path (uri), 
					 SVN_PROPNAME_KEYFRAME, tagname,
					 pool);
	if (error) g_error ("error setting keyframe: %s", error->message);
	
	tagname = svn_string_create (svn_time_to_cstring (apr_time_now (), pool), pool);
	error = svn_fs_change_node_prop (root, gnome_vfs_uri_get_path (uri),
					 SVN_PROPNAME_DATE, tagname,
					 pool);
	if (error) g_error ("error setting date for keyframe: %s", error->message);

	error = svn_fs_commit_txn (NULL, &head, txn, pool);
	if (error) g_error ("error setting keyframe: %s", error->message);
	
	g_debug ("created new keyframe revision %i", head);
	
	svn_pool_destroy (pool);

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_tag (const gchar *name, GnomeVFSURI *uri)
{
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_revnum_t head;
	svn_string_t *tagname;
	svn_fs_txn_t *txn;
	svn_fs_root_t *root;
	apr_pool_t *pool;
	svn_error_t *error;
	SVNRepository *repo;

#ifdef SVN_VFS_DEBUG
	g_debug ("tagging repo %s with tag \"%s\"", URI_TO_STRING (uri), name);
#endif
	
	repo = svn_repository_from_uri (uri);

	pool = svn_pool_create (NULL);

	/* Acquire the svn filesystem */
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), pool);
	if (error) g_debug ("error opening repo: %s", error->message);
	fs = svn_repos_fs (repos);

	/* Get the revision for head */
	error = svn_fs_youngest_rev (&head, fs, pool);
	error = svn_fs_begin_txn (&txn, fs, head, pool);
	error = svn_fs_txn_root (&root, txn, pool);

	/* Create a string for the tag */
	tagname = svn_string_create (name, pool);

	/* Name the most recent revision */
	error = svn_fs_change_node_prop (root, gnome_vfs_uri_get_path (uri),
					 SVN_PROPNAME_TAG, tagname,
					 pool);
	error = svn_fs_commit_txn (NULL, &head, txn, pool);
	svn_pool_destroy (pool);

	/* FIXME: Check for subversion error */

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_start_txn (SVNRepository *repo)
{
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_revnum_t head;
	svn_error_t *error;

	/* if we have a pre-existing transaction, just destroy the pool it exists in,
	   and it should just dissapear */
	if (repo->pool)	svn_pool_destroy (repo->pool);

	repo->pool = svn_pool_create (NULL);
	
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), repo->pool);
	if (error) g_error ("error opening repository: %s", error->message);
	
	fs = svn_repos_fs (repos);
	error = svn_fs_youngest_rev (&head, fs, repo->pool);
	if (error) g_error ("error getting youngest revision: %s", error->message);
	
	error = svn_fs_begin_txn (&(repo->txn), fs, head, repo->pool);
	if (error) g_error ("error starting transaction: %s", error->message);

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_commit_txn (SVNRepository *repo)
{
	svn_repos_t *repos;
	svn_revnum_t head;
	const gchar *conflicts;
	svn_error_t *error;

#ifdef SVN_VFS_DEBUG
	g_debug ("committing to repo");
#endif

	/* FIXME: what do we do if there is not transaction to commit? */
	if (repo->txn == NULL)
		return GNOME_VFS_OK;

	/* Acquire the svn filesystem */
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), repo->pool);
	if (error) g_error ("error opening repository: %s", error->message);

	/* Run the actual commit */
	error = svn_repos_fs_commit_txn (&conflicts, repos, &head, repo->txn, repo->pool);
	if (error) g_error ("error running commit: %s", error->message);

	/*	svn_pool_destroy (repo->pool); */

	/* FIXME: Check for subversion errors */

	return GNOME_VFS_OK;
}

NRAction
svn_action_to_nr_action (svn_wc_notify_action_t action)
{
	NRAction ret;

	if (action == svn_wc_notify_commit_added)
		ret = NR_ADDED;
	else if (action == svn_wc_notify_commit_postfix_txdelta)
		ret = NR_DELTA;
	else
		ret = NR_UNKNOWN;

	return ret;
}

typedef struct
{
	nr_notify_cb cb;
	void *baton;
} svn_import_cb_baton;

static void 
svn_import_cb (void *baton,
	       const svn_wc_notify_t *notify,
	       apr_pool_t *pool)
{
	svn_import_cb_baton *import_baton = (svn_import_cb_baton*) baton;
	g_debug ("svn_import_cb called for %s", notify->path);
	(import_baton->cb) (notify->path, svn_action_to_nr_action (notify->action), import_baton->baton);
}

GnomeVFSResult 
svn_import (GnomeVFSURI *src_uri, 
	    GnomeVFSURI *repo_uri,
	    SVNRepository **repo,
	    nr_notify_cb *notify,
	    void *baton)
{
	svn_client_commit_info_t *commit_info;
	svn_client_ctx_t *ctx;
	apr_pool_t *pool;
	svn_error_t *svn_err;
	SVNRepository *new_repo;
	GnomeVFSURI *repo_location;
	const gchar *repo_path;
	svn_import_cb_baton *cb_baton;

#ifdef SVN_VFS_DEBUG
	g_debug ("importing directory %s to repo @ %s", 
		 gnome_vfs_uri_get_path (src_uri),
		 gnome_vfs_uri_get_path (repo_uri));
#endif

	pool = svn_pool_create (NULL);
	
#ifdef SVN_VFS_DEBUG
	g_debug ("creating context");
#endif

	/* create the context */
	svn_err = svn_client_create_context (&ctx, pool);
	ctx->log_msg_func = NULL;

	/* create the notification function (if requested) */
	if (notify == NULL)
		ctx->notify_func2 = NULL;
	else
		{
			g_debug ("adding notify function");
			ctx->notify_func2 = svn_import_cb;
			cb_baton = malloc (sizeof (svn_import_cb_baton));
			cb_baton->cb = notify;
			cb_baton->baton = baton;
			ctx->notify_baton2 = cb_baton;
		}

	repo_location = gnome_vfs_uri_append_path (repo_uri, gnome_vfs_uri_get_path (src_uri));

#ifdef SVN_VFS_DEBUG
	g_debug ("context created, importing %s to %s", gnome_vfs_uri_get_path (src_uri),
		 gnome_vfs_uri_get_path (repo_location));
#endif
	repo_path = URI_TO_STRING (repo_location);
	svn_err = svn_client_import (&commit_info, gnome_vfs_uri_get_path (src_uri),
				     repo_path, false, ctx,
				     pool);

	/*g_free (repo_path);*/

#ifdef SVN_VFS_DEBUG
	g_debug ("import finished");
#endif

	if (svn_err)
		g_error ("error in importing to repository: %s", svn_err->message);

	/* FIXME: check for error in svn_err */
	/* FIXME: handle errors gracefully */

#ifdef SVN_VFS_DEBUG
	g_debug ("beginning prop import");
#endif
	if (svn_prop_import (src_uri, repo_uri) != GNOME_VFS_OK)
		g_error ("couldn't import properties!");
	/* FIXME: race condition - what happens if something changes between import & prop sets? */

	svn_pool_destroy (pool);

	new_repo = svn_repo_new ();
	new_repo->directory = src_uri;
	new_repo->repository = repo_uri;

	/* increase ref counts */
	gnome_vfs_uri_ref (src_uri);
	gnome_vfs_uri_ref (repo_uri);

	return GNOME_VFS_OK;
}

/* Get info about the repository */
GList* svn_get_recent_revisions (SVNRepository *repo, GnomeVFSURI *uri, gint number)
{
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_fs_root_t *root, *proproot;
	svn_revnum_t head, rev;
	svn_error_t *error;
	svn_fs_history_t *history, *log;
	apr_pool_t *pool;
	const gchar *path;
	svn_string_t *node;
	Revision *noderev;
	GList *revs = NULL;
	gint num = 0;

	rcs_init ();
	svn_init ();

#ifdef SVN_VFS_DEBUG
	g_debug ("retrieving revisions");
#endif
	pool = svn_pool_create (NULL);
	
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), pool);
	if (error) g_error ("error opening repository: %s", error->message);
	
	fs = svn_repos_fs (repos);
	error = svn_fs_youngest_rev (&head, fs, pool);
	if (error) g_error ("error getting youngest revision: %s", error->message);
	
	error = svn_fs_revision_root (&root, fs, head, pool);
	error = svn_fs_node_history (&log, root, gnome_vfs_uri_get_path (uri), pool);

	error = svn_fs_history_prev (&history, log, TRUE, pool);

	while (history && num < number)
		{
			num ++;
			error = svn_fs_history_location (&path, &rev, history, pool);
			error = svn_fs_revision_root (&proproot, fs, rev, pool);
			error = svn_fs_node_prop (&node, proproot, gnome_vfs_uri_get_path (uri), 
						  SVN_PROPNAME_KEYFRAME, pool);
			if (node)
				{
					g_debug ("path: %s, rev: %d", path, rev);
					noderev = rcs_revision_new ();
					noderev->number = rev;

					g_debug ("rev: %d", noderev->number);

					/* get the revision name (if applicable) */
					error = svn_fs_node_prop (&node, proproot, gnome_vfs_uri_get_path (uri),
								  SVN_PROPNAME_TAG, pool);
					if (node)
						{
							noderev->named = true;
							noderev->name = g_strdup (node->data);
						}
					else
						noderev->named = false;

					/* get the revision date */
					error = svn_fs_node_prop (&node, proproot, gnome_vfs_uri_get_path (uri),
								  SVN_PROPNAME_DATE, pool);
					noderev->date = g_strdup (node->data);

					revs = g_list_append (revs, noderev);

				}

			log = history;
			error = svn_fs_history_prev (&history, log, TRUE, pool);
		}
	
	svn_pool_destroy (pool);

	return revs;
}

/* Get info about the repository */
GList* svn_get_revisions (SVNRepository *repo, GnomeVFSURI *uri)
{
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_fs_root_t *root, *proproot;
	svn_revnum_t head, rev;
	svn_error_t *error;
	svn_fs_history_t *history, *log;
	apr_pool_t *pool;
	const gchar *path;
	svn_string_t *node;
	Revision *noderev;
	GList *revs = NULL;

	rcs_init ();
	svn_init ();

#ifdef SVN_VFS_DEBUG
	g_debug ("retrieving revisions");
#endif
	pool = svn_pool_create (NULL);
	
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), pool);
	if (error) g_error ("error opening repository: %s", error->message);
	
	fs = svn_repos_fs (repos);
	error = svn_fs_youngest_rev (&head, fs, pool);
	if (error) g_error ("error getting youngest revision: %s", error->message);
	
	error = svn_fs_revision_root (&root, fs, head, pool);
	error = svn_fs_node_history (&log, root, gnome_vfs_uri_get_path (uri), pool);

	error = svn_fs_history_prev (&history, log, TRUE, pool);

	while (history)
		{
			error = svn_fs_history_location (&path, &rev, history, pool);
			error = svn_fs_revision_root (&proproot, fs, rev, pool);
			error = svn_fs_node_prop (&node, proproot, gnome_vfs_uri_get_path (uri), 
						  SVN_PROPNAME_KEYFRAME, pool);
			if (node)
				{
					g_debug ("path: %s, rev: %d", path, rev);
					noderev = rcs_revision_new ();
					noderev->number = rev;

					g_debug ("rev: %d", noderev->number);

					/* get the revision name (if applicable) */
					error = svn_fs_node_prop (&node, proproot, gnome_vfs_uri_get_path (uri),
								  SVN_PROPNAME_TAG, pool);
					if (node)
						{
							noderev->named = true;
							noderev->name = g_strdup (node->data);
						}
					else
						noderev->named = false;

					/* get the revision date */
					error = svn_fs_node_prop (&node, proproot, gnome_vfs_uri_get_path (uri),
								  SVN_PROPNAME_DATE, pool);
					noderev->date = g_strdup (node->data);

					revs = g_list_append (revs, noderev);

				}

			log = history;
			error = svn_fs_history_prev (&history, log, TRUE, pool);
		}
	
	svn_pool_destroy (pool);

	return revs;
}

RevisionedObject* svn_entry_to_robj (SVNRepository *repo, svn_fs_root_t *root, 
				     Revision *revision, gchar *rel_path, 
				     apr_pool_t *pool)
{
	RevisionedObject *ret;

#ifdef SVN_VFS_DEBUG
	g_debug ("converting svn entry @ %s to revisioned object", rel_path);
#endif

	ret = rcs_robj_new ();

	/* Build the uri and revision */
	ret->uri = gnome_vfs_uri_new (rel_path);
	ret->revision = revision;
     
	ret->file_info = svn_get_single_fileinfo (root, rel_path, pool);
	
#ifdef SVN_VFS_DEBUG
	g_debug ("ret->file_info->name: %s", ret->file_info->name);
#endif

	return ret;
}

GList* svn_get_filelist (SVNRepository *repo, Revision *revision, gchar *rel_path)
{
	apr_hash_t *entries;
	apr_hash_index_t *entry;
	GList *rev_entries;

	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_fs_root_t *root;
	svn_revnum_t rev;
	svn_error_t *error;
	svn_fs_dirent_t *val;
	
	gchar *temp;

	apr_pool_t *pool;

#ifdef SVN_VFS_DEBUG
	g_debug ("retrieving file-info for files in folder %s", rel_path);
#endif

	pool = svn_pool_create (NULL);

	/* Acquire the svn filesystem */
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), pool);

	if (error)
		g_error ("error in opening repository: %s", error->message);

	fs = svn_repos_fs (repos);

#ifdef SVN_VFS_DEBUG
	g_debug ("Acquired svn filesystem");
#endif

	/* Acquire the root filesystem */
	rev = revision->number;

#ifdef SVN_VFS_DEBUG
	g_debug ("Acquiring root for revision %d", rev);
#endif
	error = svn_fs_revision_root (&root, fs, rev, pool);

#ifdef SVN_VFS_DEBUG
	g_debug ("finished setting up svn access stuff");
#endif

	/* Get the entry listing */
	error = svn_fs_dir_entries (&entries, root, rel_path, pool);

	rev_entries = NULL;
	for (entry = apr_hash_first (pool, entries); entry; entry = apr_hash_next (entry))
		{
			apr_hash_this (entry, NULL, NULL, &val);
#ifdef SVN_VFS_DEBUG
			g_debug ("looking at entry %s/%s", rel_path, val->name);
#endif

			temp = g_strjoin ("/", rel_path, val->name, NULL);
			rev_entries = g_list_append 
				(rev_entries, svn_entry_to_robj (repo, root, revision, 
								 temp, pool));

			g_free (temp);
#ifdef SVN_VFS_DEBUG
			g_debug ("done with entry");
#endif
		}
	
#ifdef SVN_VFS_DEBUG
	g_debug ("finished listing files");
#endif

	svn_pool_destroy (pool);

	return rev_entries;
}

GList* svn_get_fileinfo (SVNRepository *repo, Revision *revision, GList *items, gchar *rel_path)
{
	GList *entry;
	GList *info = NULL;	
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_fs_root_t *root;
	svn_revnum_t rev;
	svn_error_t *error;

	apr_pool_t *pool;

	pool = svn_pool_create (NULL);

	/* Acquire the svn filesystem */
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), pool);
	if (error)
		g_error ("error in opening repository: %s", error->message);
	fs = svn_repos_fs (repos);
	rev = revision->number;
	error = svn_fs_revision_root (&root, fs, rev, pool);
	

#ifdef SVN_VFS_DEBUG
	g_debug ("retrieving file-info");
#endif

	for (entry = items; entry; entry = g_list_next (entry))
		info = g_list_append (info, 
				      svn_get_single_fileinfo (root, g_strjoin (rel_path, (svn_fs_dirent_t*)entry->data, NULL), pool));

	svn_pool_destroy (pool);

	return info;
}

/* Individual operations on files or directories*/
GnomeVFSURI* svn_get_file (SVNRepository *repo, RevisionedObject *obj)
{
#ifdef SVN_VFS_DEBUG
	g_debug ("retrieving file %s", gnome_vfs_uri_get_path (obj->uri));
#endif

	return NULL;
}

GnomeVFSResult svn_make_new_directory (SVNRepository *repo, GnomeVFSURI *uri)
{
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_fs_root_t *root;
	GnomeVFSResult result;
	svn_error_t *error;

	/* need to check that the uri passed in is valid */

#ifdef SVN_VFS_DEBUG
	g_debug ("creating new directory @ %s", gnome_vfs_uri_get_path (uri));
#endif
	result = svn_start_txn (repo);
	if (result != GNOME_VFS_OK) g_error ("svn_start_txn failed!");

	/* Set up the svn variables */
	error = svn_fs_txn_root (&root, repo->txn, repo->pool);
	if (error) g_error ("error acquiring txn root: %s", error->message);

	/* Create the directory */
	error = svn_fs_make_dir (root, gnome_vfs_uri_get_path (uri), repo->pool);
	if (error) g_error ("error creating directory: %s", error->message);

	/* Set attributes on the directory */
	result = svn_prop_import_individual (root, gnome_vfs_uri_get_path (uri), repo->pool);
	if (result != GNOME_VFS_OK) g_error ("couldn't set properties on new dir!");

	result = svn_commit_txn (repo);
	if (result != GNOME_VFS_OK) g_error ("failed to commit transaction!");

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_cp (SVNRepository *repo, RevisionedObject *obj, GnomeVFSURI *new_uri)
{
#ifdef SVN_VFS_DEBUG
	g_debug ("copying file %s to %s",
		 gnome_vfs_uri_get_path (obj->uri),
		 gnome_vfs_uri_get_path (new_uri));
#endif

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_mv (SVNRepository *repo, RevisionedObject *obj, GnomeVFSURI *new_uri)
{
#ifdef SVN_VFS_DEBUG
	g_debug ("moving file %s to %s",
		 gnome_vfs_uri_get_path (obj->uri),
		 gnome_vfs_uri_get_path (new_uri));
#endif

	return GNOME_VFS_OK;
}
