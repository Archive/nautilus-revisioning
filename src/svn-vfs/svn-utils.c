/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* svn-utils.c: Utilities for working with svn types */

#include "svn-utils.h"
#include "svn-interface.h"
#include <svn_pools.h>

gint g_list_str_equal (gconstpointer a, gconstpointer b)
{
	if (g_str_equal (a, b)) return 0;
	return 1;
}

gchar* svn_cleanup_path (const gchar *path)
{
	GString *ret;

	if (g_str_has_suffix (path, "/"))
		{
#ifdef SVN_UTIL_DEBUG
			g_debug ("string %s has suffix '/'\n", path);
#endif
			ret = g_string_new (path);
			ret = g_string_truncate (ret, ret->len - 1);
			return g_string_free (ret, FALSE);
		}

	return g_strdup (path);
}

SVNRepository* svn_repository_from_uri (GnomeVFSURI *uri)
{
	GSList *dirs, *first;
	SVNRepository *repo;
	GnomeVFSURI *local;

#ifdef SVN_UTIL_DEBUG
	g_debug ("extracting repo from uri %s\n", URI_TO_STRING (uri));
#endif

	/* remove the revision */
	dirs = rcs_gconf_directories ();
	first = dirs;

	for (;dirs;dirs = g_slist_next (dirs))
		dirs->data = gnome_vfs_uri_new (svn_cleanup_path (dirs->data));

	dirs = first;
	local = gnome_vfs_uri_new (svn_cleanup_path (gnome_vfs_uri_get_path (uri)));

	/* FIXME: what happens if we don't find a uri? */
	while (dirs && ! (gnome_vfs_uri_equal (dirs->data, local) ||
			  gnome_vfs_uri_is_parent (dirs->data, local, TRUE)))
		{
#ifdef SVN_UTIL_DEBUG
			g_debug ("%s is not in %s\n", gnome_vfs_uri_get_path (uri), gnome_vfs_uri_get_path (dirs->data));
#endif
			dirs = g_slist_next (dirs);
		}

	if (dirs == NULL)
		{ 
			g_debug ("%s not found in managed dirs\n", gnome_vfs_uri_get_path (local));
			return NULL;
		}
#ifdef SVN_UTIL_DEBUG
	else g_debug ("%s found in managed dirs\n", 
		      gnome_vfs_uri_get_path (dirs->data));
#endif

	repo = svn_repo_new ();
	repo->directory = dirs->data;
	repo->repository = gnome_vfs_uri_new (rcs_gconf_repository());
     
	repo->txn = NULL;

	g_slist_free (dirs);
	/* FIXME: need to free strings in dirs too */

#ifdef SVN_UTIL_DEBUG
	g_debug ("New repo:\n\tDirectory = %s\n\tRepository = %s\n", 
		 URI_TO_STRING (repo->directory),
		 URI_TO_STRING (repo->repository));
#endif

	return repo;
}

gchar* svn_get_rel_path_from_uri (SVNRepository *repo, GnomeVFSURI *uri)
{
	gchar **ret;
#ifdef SVN_UTIL_DEBUG
	g_debug ("extracting relative path from uri %s\n", URI_TO_STRING (uri));
#endif

	ret = g_strsplit (gnome_vfs_uri_get_path (uri), gnome_vfs_uri_get_path (repo->directory), 1);
	
	return *ret;
}

Revision* svn_get_revision_from_uri (SVNRepository *repo, GnomeVFSURI *uri)
{
	const gchar *rev;
	gchar *end;
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_error_t *error;
	apr_pool_t *pool;
	svn_revnum_t head;
	gchar *repo_path;
	/*	svn_string_t *name;
	  svn_revnum_t svn_rev;
	  svn_fs_t *fs;
	  svn_repos_t *repos; */
	Revision *revision;

	/*	svn_error_t *error; */

#ifdef SVN_UTIL_DEBUG
	g_debug ("extracting revision from uri %s\n", URI_TO_STRING (uri));
#endif
	
	rev = gnome_vfs_uri_get_host_name (uri);
	
	/* error = svn_fs_revision_prop (&name, fs, svn_rev, SVN_INTERFACE_PROPNAME_TAG, pool);
	   revision->named = (name != 0);
	   revision->name = name;
	   
	   revision->date = g_date_new ();
	   g_date_set_time (revision->date,  */

	revision = rcs_revision_new ();
	revision->named = FALSE;
	if (rev)
		revision->number = g_ascii_strtod (rev, &end);
	else
		{
			pool = svn_pool_create (NULL);
			repo_path = rcs_gconf_repository ();
			error = svn_repos_open (&repos, repo_path, pool);
			if (error) g_error ("error opening repository!\n");
			g_free (repo_path);
			fs = svn_repos_fs (repos);
			
			error = svn_fs_youngest_rev (&head, fs, pool);
			if (error) g_error ("error getting youngest revision");

			revision->number = (double)head;
			
			svn_pool_destroy (pool);
		}


#ifdef SVN_UTIL_DEBUG
	g_debug ("New Revision:\n\tNumber: %s\n", rev);
#endif

	/*	g_free (rev); */

	return revision;
}

gboolean revision_is_youngest (Revision *revision)
{
	svn_repos_t *repos;
	svn_fs_t *fs;
	svn_revnum_t head;
	svn_error_t *error;
	apr_pool_t *pool;
	gchar *repo_path;

	pool = svn_pool_create (NULL);

	repo_path = rcs_gconf_repository ();
	error = svn_repos_open (&repos, repo_path, pool);
	if (error) g_error ("error opening repository!\n");
	g_free (repo_path);
	fs = svn_repos_fs (repos);

	error = svn_fs_youngest_rev (&head, fs, pool);
	if (error) g_error ("error getting youngest revision");
	
	svn_pool_destroy (pool);

#ifdef SVN_UTIL_DEBUG
	g_debug ("youngest revision is %d, requested is %d\n", 
		 head, revision->number);
#endif

	return (revision->number == head);
}

SVNRepository* svn_repo_new ()
{
	SVNRepository *repo;
	
	repo = (SVNRepository*) g_malloc (sizeof (SVNRepository));
	
	repo->directory = NULL;
	repo->repository = NULL;
	repo->txn = NULL;
	repo->pool = NULL;

	return repo;
}

SVNFileHandle* svn_handle_new ()
{
	SVNFileHandle *handle;

	handle = (SVNFileHandle*) g_malloc (sizeof (SVNFileHandle));

	handle->object = NULL;
	handle->root = NULL;
	handle->txn = NULL;
	handle->contents = NULL;
	handle->pool = NULL;
}

void svn_repo_free (SVNRepository *repo)
{
	svn_error_t *error;

	if (repo->directory) gnome_vfs_uri_unref (repo->directory);
	if (repo->repository) gnome_vfs_uri_unref (repo->repository);
	
	if (repo->txn) error = svn_fs_abort_txn (repo->txn, repo->pool);
	if (repo->pool) svn_pool_destroy (repo->pool);
}

void svn_handle_free (SVNFileHandle *handle)
{
	if (handle->object) rcs_robj_free (handle->object);
	/* destroying the pool will deal with all the subversion types */
	if (handle->pool) svn_pool_destroy (handle->pool);

	/*	g_free (handle); */
}

GnomeVFSFileInfo* svn_get_file_info_from_uri (GnomeVFSURI *uri)
{
	svn_fs_root_t *root;
	svn_fs_t *fs;
	svn_repos_t *repos;
	svn_error_t *error;
	svn_node_kind_t kind;
	apr_pool_t *pool;
	GnomeVFSFileInfo *file_info;

#ifdef SVN_UTIL_DEBUG
	g_debug ("getting file info from uri %s\n", URI_TO_STRING (uri));
#endif

	pool = svn_pool_create (NULL);

	error = svn_repos_open (&repos, rcs_gconf_repository (), pool);
	if (error) g_error ("Couldn't open repos: %s\n", error->message);
	fs = svn_repos_fs (repos);
	error = svn_fs_revision_root (&root, fs, g_ascii_strtod (gnome_vfs_uri_get_host_name (uri), NULL), pool);
	if (error) g_error ("Couldn't open revision root: %s\n", error->message);

	error = svn_fs_check_path (&kind, root, gnome_vfs_uri_get_path (uri), pool);
	if (error) g_error ("Couldn't get file status: %s\n", error->message);
	if (kind == svn_node_none)
		{
			svn_pool_destroy (pool);
			return NULL;
		}

	file_info = svn_get_single_fileinfo (root, gnome_vfs_uri_get_path (uri), pool);
	
	svn_pool_destroy (pool);
	
	return file_info;
}

RevisionedObject* svn_get_revisioned_object_from_uri (GnomeVFSURI *uri)
{
	SVNRepository *repo;
	RevisionedObject *robj;
	svn_fs_root_t *root;
	svn_fs_t *fs;
	svn_repos_t *repos;
	svn_error_t *error;
	gchar *rel_path;
	svn_node_kind_t kind;
	apr_pool_t *pool;

#ifdef SVN_UTIL_DEBUG
	g_debug ("getting revisioned object from uri %s\n", URI_TO_STRING (uri));
#endif

	repo = svn_repository_from_uri (uri);
	if (repo == NULL) return NULL;

	robj = rcs_robj_new ();
	robj->revision = svn_get_revision_from_uri (repo, uri);
	robj->uri = uri;
	gnome_vfs_uri_ref (uri);

	pool = svn_pool_create (NULL);

	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo->repository), pool);
	if (error) g_error ("Couldn't open repos: %s\n", error->message);
	fs = svn_repos_fs (repos);
	error = svn_fs_revision_root (&root, fs, robj->revision->number, pool);
	if (error) g_error ("Couldn't open revision root: %s\n", error->message);

	error = svn_fs_check_path (&kind, root, gnome_vfs_uri_get_path (uri), pool);
	if (error) g_error ("Couldn't get file status: %s\n", error->message);
	if (kind == svn_node_none)
		{
			svn_pool_destroy (pool);
			rcs_revision_free (robj->revision);
			rcs_robj_free (robj);
			
			return NULL;
		}

	rel_path = svn_get_rel_path_from_uri (repo, uri);
	robj->file_info = svn_get_single_fileinfo (root, rel_path, pool);
	g_free (rel_path);
	
	svn_pool_destroy (pool);
	
	return robj;
}
