/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* nr-import.c: import a directory into the rcs repo & set it up for gnomevfs
   access */

#include "svn-import.h"
#include <glib.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>

int 
usage (char *prog)
{
	printf ("Usage: %s <path>\n\tpath: path to import into repo\n", prog);
	
	return 0;
}

static void
nr_import_cb (const gchar *path,
	      NRAction     action,
	      void        *baton)
{
	GtkProgressBar *progress = (GtkProgressBar*) baton;

	gtk_progress_bar_pulse (progress);
	gtk_progress_bar_set_text (progress, path);

	if (action == NR_ADDED)
		g_debug ("added %s", path);
	if (action == NR_DELTA)
		g_debug ("delta for %s", path);
}

static void
import_folder (GtkWidget *widget,
	       gpointer   data)
{
	const gchar *path;
	GtkWidget *window, *progress;

	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_widget_show (window);
	gtk_window_set_title (GTK_WINDOW (window), "Import Progress");
	gtk_container_set_border_width (GTK_CONTAINER (window), 12);

	g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (gtk_main_quit), NULL);

	progress = gtk_progress_bar_new ();
	gtk_container_add (GTK_CONTAINER (window), progress);
	gtk_widget_show (progress);	
	gtk_progress_bar_pulse (progress);
	
	path = gtk_file_chooser_get_uri (GTK_FILE_CHOOSER (data));
	nr_import (path, (nr_notify_cb*) nr_import_cb, (void*)progress);
}

int 
main (int    argc, 
      char **argv)
{
	GtkWidget *window, *location;
	GtkWidget *hbox, *vbox, *label, *button;
	
	gtk_init (&argc, &argv);
	
	rcs_init ();
	svn_init ();
	
	if (! gnome_vfs_init ())
		g_error ("couldn't init gnomevfs!\n");

	/* Get the location */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_widget_show (window);
	gtk_window_set_title (GTK_WINDOW (window), "Choose a Folder to Manage");
	gtk_container_set_border_width (GTK_CONTAINER (window), 12);

	g_signal_connect (G_OBJECT (window), "destroy", G_CALLBACK (gtk_main_quit), NULL);

	vbox = gtk_vbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (window), vbox);
	gtk_widget_show (vbox);

	hbox = gtk_hbox_new (FALSE, 6);
	gtk_container_add (GTK_CONTAINER (vbox), hbox);
	gtk_widget_show (hbox);

	label = gtk_label_new ("Folder: ");
	gtk_container_add (GTK_CONTAINER (hbox), label);
	gtk_widget_show (label);

	location = gtk_file_chooser_button_new ("Choose a folder", GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
	gtk_container_add (GTK_CONTAINER (hbox), location);
	gtk_widget_show (location);

	button = gtk_button_new_from_stock (GTK_STOCK_OPEN);
	gtk_container_add (GTK_CONTAINER (vbox), button);
	gtk_widget_show (button);

	g_signal_connect (G_OBJECT (button), "clicked", G_CALLBACK (import_folder), location);
	
	gtk_main ();

	svn_shutdown ();
	gnome_vfs_shutdown ();

}

