/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

#include "svn-prop.h"
#include <svn_pools.h>

/* svn-prop.c: Utilities for working with svn properties */

gboolean svn_valid (GnomeVFSFileInfoFields valid, const char *name)
{
#ifdef SVN_PROP_DEBUG
	g_debug ("checking if prop %s is valid\n", name);
	g_debug ("valid: %x\n", valid);
#endif

	if (name == SVN_PROPNAME_NAME || name == SVN_PROPNAME_VALIDFIELDS)
		return TRUE;

	if (valid == GNOME_VFS_FILE_INFO_FIELDS_NONE)
		return FALSE;

	if (name == SVN_PROPNAME_TYPE)
		return (GNOME_VFS_FILE_INFO_FIELDS_TYPE == GNOME_VFS_FILE_INFO_FIELDS_TYPE & valid);

	if (name == SVN_PROPNAME_PERMS || name == SVN_PROPNAME_UID || name == SVN_PROPNAME_GID)
		return (GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS == GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS & valid);

	if (name == SVN_PROPNAME_FLAGS)
		return (GNOME_VFS_FILE_INFO_FIELDS_FLAGS == GNOME_VFS_FILE_INFO_FIELDS_FLAGS & valid);
	
	if (name == SVN_PROPNAME_SIZE)
		return (GNOME_VFS_FILE_INFO_FIELDS_SIZE == GNOME_VFS_FILE_INFO_FIELDS_SIZE & valid);
	
	if (name == SVN_PROPNAME_BLOCKS)
		return (GNOME_VFS_FILE_INFO_FIELDS_BLOCK_COUNT == GNOME_VFS_FILE_INFO_FIELDS_SIZE & valid);

	if (name == SVN_PROPNAME_ATIME)
		return (GNOME_VFS_FILE_INFO_FIELDS_ATIME == GNOME_VFS_FILE_INFO_FIELDS_ATIME & valid);

	if (name == SVN_PROPNAME_MTIME)
		return (GNOME_VFS_FILE_INFO_FIELDS_MTIME == GNOME_VFS_FILE_INFO_FIELDS_MTIME & valid);

	if (name == SVN_PROPNAME_CTIME)
		return (GNOME_VFS_FILE_INFO_FIELDS_CTIME == GNOME_VFS_FILE_INFO_FIELDS_CTIME & valid);

	if (name == SVN_PROPNAME_SYMLINK)
		return (GNOME_VFS_FILE_INFO_FIELDS_SYMLINK_NAME == GNOME_VFS_FILE_INFO_FIELDS_SYMLINK_NAME & valid);
	
	if (name == SVN_PROPNAME_MIME)
		return (GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE == GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE & valid);

	/* any props we don't want to deal with */
	return FALSE;
}

char* svn_get_prop_char (const char *name, const char *path, 
			 svn_fs_root_t *fs, apr_pool_t *pool,
			 GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	gchar *ret;
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Fetching char* prop %s for file %s\n", name, path);
#endif

	if (!svn_valid (valid_fields, name))
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid\n", name);
#endif
			return NULL;
		}
	
#ifdef SVN_PROP_DEBUG
	g_debug ("prop is valid\n");
#endif

	subpool = svn_pool_create (pool);

	error = svn_fs_node_prop (&str, fs, path, name, subpool);
	if (str == NULL)
		ret = NULL;
	else
		ret = g_strdup (str->data);

#ifdef SVN_PROP_DEBUG
	g_debug ("prop value is \"%s\"\n", ret);
#endif

	svn_pool_destroy (subpool);

	return ret;
}

guint svn_get_prop_guint (const char *name, const char *path, 
			  svn_fs_root_t *fs, apr_pool_t *pool,
			  GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	guint ret;
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Fetching guint prop %s for file %s\n", name, path);
#endif

	if (! svn_valid (valid_fields, name))
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid\n", name);
#endif
			return 0;
		}

	subpool = svn_pool_create (pool);

	error = svn_fs_node_prop (&str, fs, path, name, subpool);
	if (error) g_error ("error occured while fetching guint property: %s\n", error->message);
	ret = (guint)g_ascii_strtod (str->data, NULL);

#ifdef SVN_PROP_DEBUG
	g_debug ("valid!\nguint value: %u\n", ret);
#endif

	svn_pool_destroy (subpool);

	return ret;
}

GnomeVFSFileSize svn_get_prop_size (const char *name, const char *path, 
				    svn_fs_root_t *fs, apr_pool_t *pool,
				    GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	GnomeVFSFileSize size;	
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Fetching size prop %s for file %s\n", name, path);
#endif

	if (! svn_valid (valid_fields, name))
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid\n", name);
#endif
			return 0;
		}

	subpool = svn_pool_create (pool);

	error = svn_fs_node_prop (&str, fs, path, name, subpool);
	size = (GnomeVFSFileSize)g_ascii_strtod (str->data, NULL);

	svn_pool_destroy (subpool);

	return size;
}

time_t svn_get_prop_time (const char *name,const char *path,
			  svn_fs_root_t *fs, apr_pool_t *pool,
			  GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	time_t ret;
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Fetching time prop %s for file %s\n", name, path);
#endif

	if (! svn_valid (valid_fields, name))
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid\n", name);
#endif
			return 0;
		}

	subpool = svn_pool_create (pool);

	error = svn_fs_node_prop (&str, fs, path, name, subpool);
	ret = (time_t)g_ascii_strtod (str->data, NULL);

	svn_pool_destroy (subpool);

	return ret;
}

GnomeVFSFileInfo* svn_get_single_fileinfo (svn_fs_root_t *fs, gchar *rel_path, apr_pool_t *pool)
{
	GnomeVFSFileInfo *ret;
	apr_pool_t *sub;

	sub = svn_pool_create (pool);

#ifdef SVN_VFS_DEBUG
	g_debug ("getting single file info for file %s\n", rel_path);
#endif
	
	/* Build the file info */
	ret = gnome_vfs_file_info_new ();
	ret->name = svn_get_prop_char (SVN_PROPNAME_NAME, rel_path, fs, sub, 0);

	ret->valid_fields = (GnomeVFSFileInfoFields)svn_get_prop_guint (SVN_PROPNAME_VALIDFIELDS, rel_path, fs, sub, ret->valid_fields);
	ret->type = (GnomeVFSFileType)svn_get_prop_guint (SVN_PROPNAME_TYPE, rel_path, fs, sub, ret->valid_fields);
	ret->permissions = (GnomeVFSFilePermissions)svn_get_prop_guint (SVN_PROPNAME_PERMS, rel_path, fs, sub, ret->valid_fields);
	ret->flags = (GnomeVFSFileFlags)svn_get_prop_guint (SVN_PROPNAME_FLAGS, rel_path, fs, sub, ret->valid_fields);

	ret->uid = svn_get_prop_guint (SVN_PROPNAME_UID, rel_path, fs, sub, ret->valid_fields);
	ret->gid = svn_get_prop_guint (SVN_PROPNAME_GID, rel_path, fs, sub, ret->valid_fields);

	ret->size = svn_get_prop_size (SVN_PROPNAME_SIZE, rel_path, fs, sub, ret->valid_fields);
	ret->block_count = svn_get_prop_size (SVN_PROPNAME_BLOCKS, rel_path, fs, sub, ret->valid_fields);

	/* FIXME: Calculate io_block_size */

	ret->atime = svn_get_prop_time (SVN_PROPNAME_ATIME, rel_path, fs, sub, ret->valid_fields);
	ret->mtime = svn_get_prop_time (SVN_PROPNAME_MTIME, rel_path, fs, sub, ret->valid_fields);
	ret->ctime = svn_get_prop_time (SVN_PROPNAME_CTIME, rel_path, fs, sub, ret->valid_fields);
	
	ret->symlink_name = svn_get_prop_char (SVN_PROPNAME_SYMLINK, rel_path, fs, sub, ret->valid_fields);
	ret->mime_type = svn_get_prop_char (SVN_PROPNAME_MIME, rel_path, fs, sub, ret->valid_fields);

#ifdef SVN_VFS_DEBUG
	if (ret->type == GNOME_VFS_FILE_TYPE_REGULAR) g_debug ("vfs: type is file\n");
#endif

	svn_pool_destroy (sub);
	
	/* FIXME: Add more info to files */
	
	return ret;
}

GnomeVFSResult svn_set_prop_char (const char *val, const char *name, const char *path, 
				  svn_fs_root_t *fs, apr_pool_t *pool,
				  GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Setting char* prop %s=%s for file %s\n", name, val, path);
#endif

	if (! svn_valid (valid_fields, name) || val == NULL)
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid!\n", name);
#endif
			return GNOME_VFS_OK;
		}

#ifdef SVN_PROP_DEBUG
	g_debug ("prop is valid\n");
#endif

	subpool = svn_pool_create (pool);

	str = svn_string_create (val, subpool);
	
#ifdef SVN_PROP_DEBUG
	g_debug ("about to set prop\n");
#endif
	error = svn_fs_change_node_prop (fs, path, name, str, subpool);	
	
	if (error)
		g_error ("set prop returned error: %s\n", error->message);

#ifdef SVN_PROP_DEBUG
	g_debug ("prop is set\n");
#endif

	svn_pool_destroy (subpool);

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_set_prop_guint (guint val, const char *name, const char *path, 
				   svn_fs_root_t *fs, apr_pool_t *pool,
				   GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Setting guint prop %s for file %s\n", name, path);
#endif

	if (! svn_valid (valid_fields, name))
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid\n", name);
#endif
			return GNOME_VFS_OK;
		}

	subpool = svn_pool_create (pool);

	str = svn_string_createf (subpool, "%u", val, NULL);
#ifdef SVN_PROP_DEBUG
	g_debug ("string value of %u is %s\n", val, str->data);
#endif
	error = svn_fs_change_node_prop (fs, path, name, str, subpool);

	svn_pool_destroy (subpool);

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_set_prop_size (GnomeVFSFileSize val, const char *name, const char *path, 
				  svn_fs_root_t *fs, apr_pool_t *pool,
				  GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Setting size prop %s for file %s\n", name, path);
#endif

	if (! svn_valid (valid_fields, name))
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid\n", name);
#endif
			return GNOME_VFS_OK;
		}

	subpool = svn_pool_create (pool);

	str = svn_string_createf (subpool, "%u", (guint)val);
	error = svn_fs_change_node_prop (fs, path, name, str, subpool);

	svn_pool_destroy (subpool);

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_set_prop_time (time_t val, const char *name, const char *path,
				  svn_fs_root_t *fs, apr_pool_t *pool,
				  GnomeVFSFileInfoFields valid_fields)
{
	svn_error_t *error;
	svn_string_t *str;
	apr_pool_t *subpool;

#ifdef SVN_PROP_DEBUG
	g_debug ("Setting time prop %s for file %s\n", name, path);
#endif

	if (! svn_valid (valid_fields, name))
		{
#ifdef SVN_PROP_DEBUG
			g_debug ("prop %s is not valid\n", name);
#endif
			return GNOME_VFS_OK;
		}
	subpool = svn_pool_create (pool);

	str = svn_string_createf (subpool, "%u", (guint)val);
	error = svn_fs_change_node_prop (fs, path, name, str, subpool);

	svn_pool_destroy (subpool);

	return GNOME_VFS_OK;
}

GnomeVFSResult svn_prop_import_individual (svn_fs_root_t *fs, const gchar *path, apr_pool_t *pool)
{
	GnomeVFSURI *file;
	GnomeVFSFileInfo *info;
	GnomeVFSResult error;

	apr_pool_t *sub;

	sub = svn_pool_create (pool);

#ifdef SVN_VFS_DEBUG
	g_debug ("setting individual property on %s", path);
#endif

	file = gnome_vfs_uri_new (path);
	info = gnome_vfs_file_info_new ();
	if (gnome_vfs_get_file_info_uri (file, info, GNOME_VFS_FILE_INFO_GET_MIME_TYPE | GNOME_VFS_FILE_INFO_GET_ACCESS_RIGHTS) != GNOME_VFS_OK)
		g_error ("couldn't get file info!\n");

	gnome_vfs_uri_unref (file);

	error = svn_set_prop_char (info->name, SVN_PROPNAME_NAME, path, fs, sub, info->valid_fields);
	error = svn_set_prop_guint ((guint)info->type, SVN_PROPNAME_TYPE, path, fs, sub, info->valid_fields);
	error = svn_set_prop_guint ((guint)info->permissions, SVN_PROPNAME_PERMS, path, fs, sub, info->valid_fields);
	error = svn_set_prop_guint ((guint)info->flags, SVN_PROPNAME_FLAGS, path, fs, sub, info->valid_fields);

	error = svn_set_prop_guint (info->uid, SVN_PROPNAME_UID, path, fs, sub, info->valid_fields);
	error = svn_set_prop_guint (info->gid, SVN_PROPNAME_GID, path, fs, sub, info->valid_fields);

	error = svn_set_prop_size (info->size, SVN_PROPNAME_SIZE, path, fs, sub, info->valid_fields);
	error = svn_set_prop_size (info->size, SVN_PROPNAME_BLOCKS, path, fs, sub, info->valid_fields);

	error = svn_set_prop_time (info->atime, SVN_PROPNAME_ATIME, path, fs, sub, info->valid_fields);
	error = svn_set_prop_time (info->mtime, SVN_PROPNAME_MTIME, path, fs, sub, info->valid_fields);
	error = svn_set_prop_time (info->ctime, SVN_PROPNAME_CTIME, path, fs, sub, info->valid_fields);

#ifdef SVN_PROP_DEBUG
	g_debug ("valid fields before symlink (%s) & mime (%s): %x\n", info->symlink_name, info->mime_type, info->valid_fields);
#endif

	if (info->symlink_name == NULL)
		info->valid_fields &= ~GNOME_VFS_FILE_INFO_FIELDS_SYMLINK_NAME;
	if (info->mime_type == NULL)
		info->valid_fields &= ~GNOME_VFS_FILE_INFO_FIELDS_MIME_TYPE;

#ifdef SVN_PROP_DEBUG
	g_debug ("valid fields after symlink & mime: %x\n", info->valid_fields);
#endif

	error = svn_set_prop_char (info->symlink_name, SVN_PROPNAME_SYMLINK, path, fs, sub, info->valid_fields);
	error = svn_set_prop_char (info->mime_type, SVN_PROPNAME_MIME, path, fs, sub, info->valid_fields);

	info->valid_fields &= ~GNOME_VFS_FILE_INFO_FIELDS_DEVICE;
	info->valid_fields &= ~GNOME_VFS_FILE_INFO_FIELDS_INODE;
	info->valid_fields &= ~GNOME_VFS_FILE_INFO_FIELDS_LINK_COUNT;
	info->valid_fields &= ~GNOME_VFS_FILE_INFO_FIELDS_IO_BLOCK_SIZE;

#ifdef SVN_PROP_DEBUG
	g_debug ("about to set validfields to %u (%x)\n", info->valid_fields, info->valid_fields);
#endif
	error = svn_set_prop_guint ((guint)(info->valid_fields), SVN_PROPNAME_VALIDFIELDS, path, fs, sub, info->valid_fields);

	svn_pool_destroy (sub);
	
	return GNOME_VFS_OK;
}

GnomeVFSResult svn_prop_import (GnomeVFSURI *src, GnomeVFSURI *repo)
{	
	svn_fs_root_t *root;
	svn_fs_t *fs;
	svn_fs_txn_t *txn;
	svn_repos_t *repos;
	svn_error_t *error;
	apr_pool_t *pool;
	apr_hash_t *entries;
	apr_hash_index_t *entry;
	svn_fs_dirent_t *val;
	svn_revnum_t head;
	gchar *rel_path;
	GList *paths;
	GnomeVFSURI *new_uri, *temp;
	SVNRepository *svnrepo;
	
	svnrepo = (SVNRepository*) g_malloc (sizeof (SVNRepository));
	svnrepo->directory = src;
	svnrepo->repository = repo;

	pool = svn_pool_create (NULL);

#ifdef SVN_VFS_DEBUG
	g_debug ("importing properties\n");
#endif
	error = svn_repos_open (&repos, gnome_vfs_uri_get_path (repo), pool);
	fs = svn_repos_fs (repos);

	error = svn_fs_youngest_rev (&head, fs, pool);
	error = svn_fs_begin_txn (&txn, fs, head, pool);
	error = svn_fs_txn_root (&root, txn, pool);

#ifdef SVN_VFS_DEBUG
	g_debug ("repo set up\n");
#endif

	temp = src;
	/* import the parent directory's info */
	while (!g_str_equal (gnome_vfs_uri_get_path (temp), "/"))
		{
			svn_prop_import_individual (root, gnome_vfs_uri_get_path (temp), pool);
			temp = gnome_vfs_uri_get_parent (temp);
		}


	/* now recursively import the content's info */
	paths = NULL;
	paths = g_list_append (paths, gnome_vfs_uri_get_path (src));

	while (paths != NULL)
		{
			rel_path = paths->data;
			paths = g_list_next (paths);
			
#ifdef SVN_VFS_DEBUG
			g_debug ("working on relative path %s\n", rel_path);
#endif
			error = svn_fs_dir_entries (&entries, root, rel_path, pool);

			for (entry = apr_hash_first (pool, entries); entry; entry = apr_hash_next (entry))
				{
					apr_hash_this (entry, NULL, NULL, &val);
#ifdef SVN_VFS_DEBUG
					g_debug ("looking at entry %s\n", val->name);
#endif

					temp = gnome_vfs_uri_new (rel_path);
					new_uri = gnome_vfs_uri_append_string (temp, val->name);
					gnome_vfs_uri_unref (temp);

					svn_prop_import_individual (root, gnome_vfs_uri_get_path (new_uri), pool);

					if (val->kind == svn_node_dir)
						{
#ifdef SVN_VFS_DEBUG
							g_debug ("enqueuing new path %s\n", gnome_vfs_uri_get_path (new_uri));
#endif
							paths = g_list_append (paths, gnome_vfs_uri_get_path (new_uri));
							svn_keyframe (new_uri);
						}
					else
						gnome_vfs_uri_unref (new_uri);
					
#ifdef SVN_VFS_DEBUG
					g_debug ("done with entry\n");
#endif
				}
			
			g_free (rel_path);
		}
	
	g_list_free (paths);
	
#ifdef SVN_VFS_DEBUG
	g_debug ("commiting transaction\n");
#endif
	error = svn_fs_commit_txn (NULL, &head, txn, pool);
#ifdef SVN_VFS_DEBUG
	g_debug ("transaction committed: new revision %u\n", head);
#endif
	
	svn_pool_destroy (pool);
	
	return GNOME_VFS_OK;
}
