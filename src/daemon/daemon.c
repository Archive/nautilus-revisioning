/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* daemon.c: implementation of monitoring daemon to sync repository
   with filesystem */

#include <glib.h>
#include <libgnomevfs/gnome-vfs.h>
#include <gconf/gconf-client.h>

#include "rcs-utils.h"
#include "inotify.h"
#include "inotify-syscalls.h"

#define NRINOTIFY_EVENTS IN_ATTRIB|IN_CLOSE_WRITE|IN_MOVE|IN_CREATE|IN_DELETE|IN_DELETE_SELF
#define NR_WAITEVENT 5000

typedef struct {
	int inotifyfd;
	const gchar *base;
	GHashTable *dirswd, *wddirs;
} MonitorData;

const gchar* nr_inotify_event_to_string (struct inotify_event *event)
{
	/*	if (event->mask & IN_MODIFY)
		g_debug ("MODIFY");
	if (event->mask & IN_ATTRIB)
		g_debug ("ATTRIB");
	if (event->mask & IN_CLOSE_WRITE)
		g_debug ("CLOSE_WRITE");
	if (event->mask & IN_MOVED_TO)
		g_debug ("MOVED_TO");
	if (event->mask & IN_MOVED_FROM)
		g_debug ("MOVED_FROM");
	if (event->mask & IN_CREATE)
		g_debug ("CREATE");
	if (event->mask & IN_DELETE)
		g_debug ("DELETE");
	if (event->mask & IN_DELETE_SELF)
		g_debug ("DELETE_SELF");
	if (event->mask & IN_UNMOUNT)
		g_debug ("UNMOUNT");
	if (event->mask & IN_Q_OVERFLOW)
		g_debug ("Q_OVERFLOW");
	if (event->mask & IN_IGNORED)
	g_debug ("IGNORED");*/

	if (event->mask & IN_MODIFY)
		return "MODIFY";
	if (event->mask & IN_ATTRIB)
		return "ATTRIB";
	if (event->mask & IN_CLOSE_WRITE)
		return "CLOSE_WRITE";
	if (event->mask & IN_MOVED_TO)
		return "MOVE_TO";
	if (event->mask & IN_MOVED_FROM)
		return "MOVED_FROM";
	if (event->mask & IN_CREATE)
		return "CREATE";
	if (event->mask & IN_DELETE)
		return "DELETE";
	if (event->mask & IN_DELETE_SELF)
		return "DELETE_SELF";
	return "UNKNOWN";
}

gboolean monitor_indiv_dir (const gchar *rel_path,
			    GnomeVFSFileInfo *info,
			    gboolean recursing_will_loop,
			    gpointer data,
			    gboolean *recurse)
{
	MonitorData *monitor_data = (MonitorData*) data;
	gchar *full;
	GnomeVFSURI *uri, *tmp;

	if (info->type == GNOME_VFS_FILE_TYPE_DIRECTORY)
		{
			*recurse = !recursing_will_loop;
			/*			g_debug ("monitor_indiv_dir");
			  g_debug ("base: %s; rel_path: %s", monitor_data->base, rel_path);*/
			tmp = gnome_vfs_uri_new (monitor_data->base);
			uri = gnome_vfs_uri_append_file_name (tmp, rel_path);
			gnome_vfs_uri_unref (tmp);

			/*			g_debug ("full: %s", gnome_vfs_uri_get_path (uri));*/
			
			if (!nr_inotify_monitor_dir (monitor_data->inotifyfd, 
						     g_strdup (gnome_vfs_uri_get_path (uri)), 
						     monitor_data->dirswd,  monitor_data->wddirs))
				g_error ("Couldn't start watch on dir \"%s\"", gnome_vfs_uri_get_path (uri));

			gnome_vfs_uri_unref (uri);
		}

	return TRUE;
}

gboolean unmonitor_indiv_dir (const gchar *rel_path,
			      GnomeVFSFileInfo *info,
			      gboolean recursing_will_loop,
			      gpointer data,
			      gboolean *recurse)
{
	MonitorData *monitor_data = (MonitorData*) data;
	gchar *full;
	GnomeVFSURI *uri, *tmp;

	if (info->type == GNOME_VFS_FILE_TYPE_DIRECTORY)
		{
			*recurse = !recursing_will_loop;
			/*			g_debug ("monitor_indiv_dir");
			  g_debug ("base: %s; rel_path: %s", monitor_data->base, rel_path);*/
			tmp = gnome_vfs_uri_new (monitor_data->base);
			uri = gnome_vfs_uri_append_file_name (tmp, rel_path);
			gnome_vfs_uri_unref (tmp);

			/*			g_debug ("full: %s", gnome_vfs_uri_get_path (uri));*/
			
			if (!nr_inotify_unmonitor_dir (monitor_data->inotifyfd, 
						       g_strdup (gnome_vfs_uri_get_path (uri)), 
						       monitor_data->wddirs,  monitor_data->dirswd))
				g_error ("Couldn't end watch on dir \"%s\"", gnome_vfs_uri_get_path (uri));
			
			gnome_vfs_uri_unref (uri);
		}

	return TRUE;
}

void nr_inotify_recursive_monitor (int inotifyfd,
				   const gchar* path,
				   GHashTable *wddirs,
				   GHashTable *dirswd)
{
	GnomeVFSResult result;
	MonitorData *monitor_data;

	monitor_data = (MonitorData*) g_malloc (sizeof (MonitorData));
	monitor_data->inotifyfd = inotifyfd;
	monitor_data->wddirs = wddirs;
	monitor_data->base = path;
	monitor_data->dirswd = dirswd;
	
	nr_inotify_monitor_dir (inotifyfd, g_strdup (path), wddirs, dirswd);
	
	result = gnome_vfs_directory_visit (path, GNOME_VFS_FILE_INFO_DEFAULT, 
					    GNOME_VFS_DIRECTORY_VISIT_LOOPCHECK,
					    (GnomeVFSDirectoryVisitFunc) monitor_indiv_dir,
					    monitor_data);
}

void nr_inotify_recursive_unmonitor (int inotifyfd,
				      const gchar* path,
				      GHashTable *wddirs,
				      GHashTable *dirswd)
{
	GnomeVFSResult result;
	MonitorData *monitor_data;

	monitor_data = (MonitorData*) g_malloc (sizeof (MonitorData));
	monitor_data->inotifyfd = inotifyfd;
	monitor_data->wddirs = wddirs;
	monitor_data->base = path;
	monitor_data->dirswd = dirswd;
	
	nr_inotify_unmonitor_dir (inotifyfd, g_strdup (path), wddirs, dirswd);
	
	result = gnome_vfs_directory_visit (path, GNOME_VFS_FILE_INFO_DEFAULT, 
					    GNOME_VFS_DIRECTORY_VISIT_LOOPCHECK,
					    (GnomeVFSDirectoryVisitFunc) unmonitor_indiv_dir,
					    monitor_data);
}

typedef struct {
	int inotifyfd;
	GHashTable *dirswd, *wddirs;
	GQueue *events, *rawevents;
	GIOChannel *source;
} nr_inotify_data;

#define BUF_LEN 16384

gboolean event_equal (struct inotify_event *a,
		      struct inotify_event *b)
{
	return (a != b && b != NULL &&
		a->wd == b->wd && a->mask == b->mask &&
		a->cookie == b->cookie && a->len == b->len &&
		g_str_equal (a->name, b->name));
}

void print_queue (gpointer data,
		  gpointer user_data)
{
	g_debug ("event: %s on %s", nr_inotify_event_to_string ((struct inotify_event*)data), 
		 ((struct inotify_event*)data)->name);
}

gboolean nr_inotify_read (GIOChannel *source,
			  GIOCondition condition,
			  gpointer data)
{
	GQueue *ret = ((nr_inotify_data*)data)->rawevents;
	gchar buf[BUF_LEN];
	guint len, i = 0, j = 0;
	GError *error = NULL;
	GIOStatus status;
	struct inotify_event *event, *event_dup;

	/*	if (g_io_channel_get_buffer_condition (source) != G_IO_IN)
	  {
	  g_debug ("nothing to read");
	  return ret;
	  }*/
	g_debug ("starting read");
	len = read (((nr_inotify_data*)data)->inotifyfd, buf, BUF_LEN);
	g_debug ("done");

	while (i < len)
		{
			event = (struct inotify_event *) &buf[i];
			event_dup = g_malloc (sizeof (struct inotify_event) + event->len);

			event_dup->wd = event->wd;
			event_dup->mask = event->mask;
			event_dup->cookie = event->cookie;
			event_dup->len = event->len;
			
			for (j = 0; j < event->len; j++)
				event_dup->name[j] = event->name[j];

			event = event_dup;
			
			if (!g_str_equal (nr_inotify_event_to_string (event), "UNKNOWN"))
				{
					g_queue_push_tail (ret, event);
					g_printf ("Pushed Event: %s\n", nr_inotify_event_to_string (event));
					g_printf ("wd=%d mask=%d cookie=%d len=%d\n",
						  event->wd, event->mask, event->cookie, event->len);
					if (event->len)
						g_printf ("name=%s\n", event->name);
				}					

			i += sizeof (struct inotify_event) + event->len;
		}

	/*	
	g_debug ("===== raw queue =====");
	g_queue_foreach (ret, print_queue, NULL);
	g_debug ("===== done ======");

	g_debug ("===== official queue =====");
	g_queue_foreach (((nr_inotify_data*)data)->events, print_queue, NULL);
	g_debug ("===== done ======");
	*/

	return TRUE;
}

/*void nr_simplifiy_event_queue (GAsyncQueue *changes)
{
	
}*/

gboolean nr_check_queue (gpointer data)
{
	int inotifyfd = ((nr_inotify_data*)data)->inotifyfd;
	GHashTable *wddirs = ((nr_inotify_data*)data)->wddirs;
	GHashTable *dirswd = ((nr_inotify_data*)data)->dirswd;
	GQueue *queuedevents = ((nr_inotify_data*)data)->events;
	GIOChannel *channel = ((nr_inotify_data*)data)->source;
	struct inotify_event *event;
	GnomeVFSURI *uri, *uri2, *tmp;
	GQueue *inotifyqueue;
	gchar *base;
	gboolean run_queue;

	g_debug ("running queue");
	
	inotifyqueue = ((nr_inotify_data*)data)->rawevents;
	run_queue = g_queue_is_empty (inotifyqueue);

	while (!g_queue_is_empty (inotifyqueue))
		g_queue_push_tail (queuedevents, g_queue_pop_head (inotifyqueue));
		

	if (run_queue)
		{
			g_debug ("===== Running queue: %i events =====", g_queue_get_length (queuedevents));

			/*			nr_simplify_event_queue (queuedevents);*/
			g_debug ("===== Simplifying queue: %i events remain =====", 
				 g_queue_get_length (queuedevents));

			while (!g_queue_is_empty (queuedevents))
				{
					event = g_queue_pop_head (queuedevents);

					g_printf ("wd=%d mask=%d cookie=%d len=%d\n",
						  event->wd, event->mask, event->cookie, event->len);
					if (event->len)
						g_printf ("name=%s\n", event->name);
					
					base = (gchar*) g_hash_table_lookup (wddirs, &(event->wd));
					/*g_debug ("base: %s", base);*/
					
					tmp = gnome_vfs_uri_new (base);
					uri = gnome_vfs_uri_append_file_name (tmp, event->name);
					
					g_debug ("Event %s on %s", nr_inotify_event_to_string (event), 
						 gnome_vfs_uri_get_path (uri));
					
					if ((event->mask & IN_CREATE || event->mask & IN_MOVED_TO) && event->mask & IN_ISDIR)
						{
							/*g_debug ("monitoring new directory: %s", 
							  gnome_vfs_uri_get_path (uri)); */
							nr_inotify_recursive_monitor 
								(inotifyfd, 
								 gnome_vfs_uri_get_path (uri), 
								 wddirs, dirswd);
						}
					
					if (event->mask & IN_MOVED_FROM && ((struct inotify_event*)g_queue_peek_head 
									    (queuedevents))->mask & IN_MOVED_TO)
						{
							uri2 = gnome_vfs_uri_append_file_name 
								(tmp, 
								 ((struct inotify_event*)
								  g_queue_peek_head (queuedevents))->name);
							nautilus_revisioning_handle_move 
								(gnome_vfs_uri_get_path (uri), 
								 gnome_vfs_uri_get_path (uri2));
							
							if (((struct inotify_event*)g_queue_peek_head 
							     (queuedevents))->mask & IN_ISDIR)
								{
									nr_inotify_recursive_monitor 
										(inotifyfd, 
										 gnome_vfs_uri_get_path (uri), 
										 wddirs, dirswd);
								}


							nautilus_revisioning_handle_keyframe (uri2);

							gnome_vfs_uri_unref (uri2);
						}
					else
						{
							nautilus_revisioning_handle_inotify_event 
								(event, gnome_vfs_uri_get_path (uri));
							
							g_debug ("===== about to keyframe =====");
							nautilus_revisioning_handle_keyframe (uri);
							g_debug ("===== keyframe done =====");
						}

					gnome_vfs_uri_unref (tmp);
					gnome_vfs_uri_unref (uri); 
				}

		}
	else
		g_debug ("===== Delaying queue run: %i events =====", g_queue_get_length (queuedevents));

	return TRUE;
}

gboolean nr_inotify_monitor_dir (int inotifyfd,
				 char *path,
				 GHashTable *wddirs,
				 GHashTable *dirswd)
{
	int *wd;

	g_debug ("monitoring dir %s", path);

	wd = (int*) g_malloc (sizeof (int));

	*wd = inotify_add_watch (inotifyfd, path, NRINOTIFY_EVENTS);
	if (*wd < 0)
		{
			g_error ("failed to watch dir %s", path);
			return FALSE;
		}

	/*	g_debug ("wd: %i; path: %s", *wd, path);*/

	g_hash_table_insert (wddirs, wd, g_strdup (path));
	g_hash_table_insert (dirswd, path, wd);

	return TRUE;
}

gboolean nr_inotify_unmonitor_dir (int inotifyfd,
				   char *path,
				   GHashTable *wddirs,
				   GHashTable *dirswd)
{
	int ret, *wd;

	g_debug ("unmonitoring dir %s", path);

	wd = g_hash_table_lookup (dirswd, path);
	ret = inotify_rm_watch (inotifyfd, *wd);
	if (ret < 0)
		{
			g_error ("failed to remove watch on dir %s", path);
			return FALSE;
		}

	g_hash_table_remove (wddirs, wd);
	g_hash_table_remove (dirswd, path);
	
	g_free (wd);

	return TRUE;
}

void nr_inotify_hash_remove (gpointer key,
			     gpointer value,
			     gpointer user_data)
{
	int ret, inotifyfd = *(int*)user_data;

	ret = inotify_rm_watch (inotifyfd, *(int*)key);
	if (ret < 0)
		g_error ("couldn't end watch");
}

void nr_inotify_end_monitors (int inotifyfd, GHashTable *dirs)
{
	g_hash_table_foreach (dirs, nr_inotify_hash_remove, &inotifyfd);
}

void gconf_ended_cb (gpointer data)
{
	GSList *list,*iter;

	list = (GSList*) data;
	for (iter = list; iter; iter = g_slist_next (iter))
		g_free (iter->data);
	g_slist_free (data);
	
}

void gconf_add_monitor_lists (GConfClient *client,
			      GSList *new,
			      GSList *orig,
			      nr_inotify_data *data)
{
	GError *err = NULL;
	gboolean success;

	for (;new;new = g_slist_next (new))
		{
			orig = g_slist_append (orig, new->data);
			nr_inotify_recursive_monitor (data->inotifyfd, new->data, data->wddirs, data->dirswd);
		}

	success = gconf_client_set_list (client, RCS_GCONF_DIRECTORIES,
					 GCONF_VALUE_STRING, orig, &err);
	success = gconf_client_set_list (client, RCS_GCONF_NEW_DIRECTORIES,
					 GCONF_VALUE_STRING, NULL, &err);
}

gint gconf_rm_test (gconstpointer data,
		    gconstpointer user_data)
{
	g_debug ("data: %s user_data: %s", (const gchar*)data, (const gchar*)user_data);

	if (g_str_equal ((const gchar*)data, (const gchar*)user_data)) return 0;
	return 1;
}

void gconf_rm_monitor_lists (GConfClient *client,
			     GSList *rm,
			     GSList *orig,
			     nr_inotify_data *data,
			     gboolean rminotify)
{
	GError *err = NULL;
	gboolean success;
	GSList *iter;

	for (;rm;rm = g_slist_next (rm))
		{
			GSList *item;

			g_debug ("removing item %s", rm->data);

			item = g_slist_find_custom (orig, rm->data, gconf_rm_test);
			orig = g_slist_delete_link (orig, item);
			if (rminotify) nr_inotify_recursive_unmonitor (data->inotifyfd, rm->data, data->wddirs, data->dirswd);
		}

	success = gconf_client_set_list (client, RCS_GCONF_DIRECTORIES,
					 GCONF_VALUE_STRING, orig, &err);
	success = gconf_client_set_list (client, RCS_GCONF_RM_DIRECTORIES,
					 GCONF_VALUE_STRING, NULL, &err);
}

void gconf_changed_cb (GConfClient *client,
		       guint cxnid,
		       GConfEntry *entry,
		       gpointer user_data)
{
	GSList *orig, *new, *iter;
	GError *err = NULL;
	nr_inotify_data *data = (nr_inotify_data*) user_data;

	g_debug ("key changed: %s", entry->key);

	if (!g_str_equal (entry->key, RCS_GCONF_NEW_DIRECTORIES) &&
	    !g_str_equal (entry->key, RCS_GCONF_RM_DIRECTORIES))
		return;

	new = gconf_value_get_list (entry->value);

	if (! new)
		return;

	orig = gconf_client_get_list (client, RCS_GCONF_DIRECTORIES,
				      GCONF_VALUE_STRING, &err);

	for (iter = new; iter; iter = g_slist_next (iter))
		{
			gpointer temp;

			temp = iter->data;
			iter->data = (gpointer)gconf_value_get_string (iter->data);
			g_free (temp);
		}

	if (g_str_equal (entry->key, RCS_GCONF_NEW_DIRECTORIES))
		gconf_add_monitor_lists (client, new, orig, data);
	else
		gconf_rm_monitor_lists (client, new, orig, data, true);
}

int main (int argc, char **argv)
{
	gint inotifyfd;
	GSList *dirs, *dir;
	GSList *new, *orig, *rm;
	guint gconfnotifyid;
	GConfClient *client;
	GError *err = NULL;
	nr_inotify_data *data;
	guint timeoutid, iochannelid;

	/*	if (argc < 2)
	  g_error ("usage: %s <paths>", argv[0]);*/

	if (! gnome_vfs_init ())
		g_error ("couldn't initialize gnome-vfs!");

	inotifyfd = inotify_init ();
	if (inotifyfd < 0)
		g_error ("inotify_init failed to return valid fd");
	/*	g_debug ("inotify connection fd: %i", inotifyfd);*/

	data = (nr_inotify_data*) g_malloc (sizeof (nr_inotify_data));
	data->inotifyfd = inotifyfd;
	data->dirswd = g_hash_table_new (g_str_hash, g_str_equal);
	data->wddirs = g_hash_table_new (g_int_hash, g_int_equal);
	data->events = g_queue_new ();
	data->source = g_io_channel_unix_new (inotifyfd);
	data->rawevents = g_queue_new ();

	iochannelid = g_io_add_watch (data->source, G_IO_IN, nr_inotify_read, data);
	timeoutid = g_timeout_add (NR_WAITEVENT, nr_check_queue, data);

	client = gconf_client_get_default ();
	gconf_client_add_dir (client, RCS_GCONF_DIRECTORY, GCONF_CLIENT_PRELOAD_NONE, &err);

	new = gconf_client_get_list (client, RCS_GCONF_NEW_DIRECTORIES,
				     GCONF_VALUE_STRING, &err);
	rm = gconf_client_get_list (client, RCS_GCONF_RM_DIRECTORIES,
				    GCONF_VALUE_STRING, &err);
	orig = gconf_client_get_list (client, RCS_GCONF_DIRECTORIES,
				      GCONF_VALUE_STRING, &err);

	gconf_add_monitor_lists (client, new, orig, data);
	gconf_rm_monitor_lists (client, rm, orig, data, false);

	dirs = gconf_client_get_list (client,
				      RCS_GCONF_DIRECTORIES,
				      GCONF_VALUE_STRING,
				      &err);

      	/* start monitoring directories */
	for (dir = dirs; dir; dir = g_slist_next (dir))
		{
			nr_inotify_recursive_monitor (inotifyfd, dir->data, data->wddirs, data->dirswd);
			g_debug ("Watching \"%s\"", dir->data);
		}

	/* monitor gconf key for changes */
	gconfnotifyid = gconf_client_notify_add (client,
						 RCS_GCONF_DIRECTORY,
						 gconf_changed_cb,
						 (gpointer)data,
						 gconf_ended_cb,
						 &err);
						 
	gtk_main ();

	nr_inotify_end_monitors (inotifyfd, data->wddirs);
	g_hash_table_destroy (data->wddirs);
	g_hash_table_destroy (data->dirswd);

	/*	g_debug ("Stopped watch");*/

	inotifyfd = close (inotifyfd);
	if (inotifyfd < 0)
		g_error ("couldn't close inotify");

	return 0;
}

