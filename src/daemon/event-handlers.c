/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* event-handlers.c: functions for responses to FAM events (sync'ing with
   repository) */

#include <glib.h>
#include <libgnomevfs/gnome-vfs.h>

#include "rcs-utils.h"

#include "inotify.h"
#include "inotify-syscalls.h"

#define NR_URI_SCHEME "svn://"

void nautilus_revisioning_handle_keyframe (GnomeVFSURI *uri)
{
	GnomeVFSHandle *handle;
	GnomeVFSResult result;
	RCSFileControlData *data;
	gchar *scheme;

	data = (RCSFileControlData*) g_malloc (sizeof (RCSFileControlData));
	data->uri = uri;
	gnome_vfs_uri_ref (uri);

	scheme = g_strconcat (NR_URI_SCHEME, gnome_vfs_uri_get_path (uri), NULL);

	g_debug ("Keyframing %s", scheme);

	g_debug ("=== opening ===");
	result = gnome_vfs_open (&handle, scheme, GNOME_VFS_OPEN_READ);
	if (result != GNOME_VFS_OK) g_error ("can't open uri %s: %s", scheme,
					     gnome_vfs_result_to_string (result));

	g_debug ("=== keyframing ===");
	result = gnome_vfs_file_control (handle, "keyframe", (gpointer)data);
	if (result != GNOME_VFS_OK) g_error ("can't keyframe uri %s: %s", scheme,
					     gnome_vfs_result_to_string (result));

	g_debug ("=== closing ===");
	result = gnome_vfs_close (handle);
	if (result != GNOME_VFS_OK) g_error ("can't close uri %s: %s", scheme,
					     gnome_vfs_result_to_string (result));

	gnome_vfs_uri_unref (uri);
}

void nautilus_revisioning_handle_move (const gchar *from, 
				       const gchar *to)
{
	gchar *fromscheme, *toscheme;
	GnomeVFSResult result;

	g_debug ("moving %s to %s", from, to);

	fromscheme = g_strconcat (NR_URI_SCHEME, from, NULL);
	toscheme = g_strconcat (NR_URI_SCHEME, to, NULL);

	g_debug ("fromscheme: %s", fromscheme);
	g_debug ("toscheme: %s", toscheme);

	result = gnome_vfs_move (fromscheme, toscheme, TRUE);

	g_debug ("Move result: %s", gnome_vfs_result_to_string (result));
}

void nautilus_revisioning_update_file_attrib (const gchar *file)
{
	gchar *scheme;
	GnomeVFSHandle *handle;
	GnomeVFSResult result;

	scheme = g_strconcat (NR_URI_SCHEME, file, NULL);
	
	result = gnome_vfs_open (&handle, scheme, GNOME_VFS_OPEN_WRITE);
	g_debug ("Open result: %s", gnome_vfs_result_to_string (result));
	result = gnome_vfs_close (handle);
	g_debug ("Close result: %s", gnome_vfs_result_to_string (result));

	g_free (scheme);
}

void nautilus_revisioning_create_file (const gchar *file)
{
	gchar *scheme;
	GnomeVFSURI *srcuri, *desturi;
	GnomeVFSResult result;

	scheme = g_strconcat (NR_URI_SCHEME, file, NULL);

	srcuri = gnome_vfs_uri_new (file);
	desturi = gnome_vfs_uri_new (scheme);

	result = gnome_vfs_xfer_uri (srcuri, desturi, 
				     GNOME_VFS_XFER_RECURSIVE | GNOME_VFS_XFER_NEW_UNIQUE_DIRECTORY,
				     GNOME_VFS_XFER_ERROR_MODE_ABORT,
				     GNOME_VFS_XFER_OVERWRITE_MODE_ABORT,
				     NULL, NULL);
	g_debug ("Create result: %s", gnome_vfs_result_to_string (result));
	g_free (scheme);
	gnome_vfs_uri_unref (srcuri);
	gnome_vfs_uri_unref (desturi);
}

void nautilus_revisioning_update_file_content (const gchar *file)
{
	nautilus_revisioning_create_file (file);
}

void nautilus_revisioning_delete_file (const gchar *file)
{
	gchar *scheme;
	GnomeVFSResult result;

	scheme = g_strconcat (NR_URI_SCHEME, file, NULL);
	result = gnome_vfs_unlink (scheme);
	g_debug ("Delete result: %s", gnome_vfs_result_to_string (result));
	g_free (scheme);
}

void nautilus_revisioning_handle_inotify_event (struct inotify_event *event, const gchar *file)
{
	if (event->mask & IN_ATTRIB)
		nautilus_revisioning_update_file_attrib (file);
	if (event->mask & IN_CLOSE_WRITE)
		nautilus_revisioning_update_file_content (file);
	if (event->mask & IN_CREATE || event->mask & IN_MOVED_TO)
		nautilus_revisioning_create_file (file);
	if (event->mask & IN_DELETE || event->mask & IN_DELETE_SELF || event->mask & IN_MOVED_FROM)
		nautilus_revisioning_delete_file (file);
}

