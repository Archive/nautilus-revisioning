/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* rcs-utils.h: general-purpose data structures and functions
   used by every rcs implementation */

#include "rcs-utils.h"
#include <gconf/gconf-client.h>

void rcs_init ()
{
	g_debug ("rcs_init called");
	g_type_init ();
}

gchar* rcs_gconf_repository ()
{
	GConfClient *client;
	gchar *raw, *ret;
	
#ifdef RCS_DEBUG
	g_debug ("rcs_gconf_repository\n");
#endif

	client = gconf_client_get_default ();
	raw = gconf_client_get_string (client, RCS_GCONF_REPOSITORY, NULL);
	ret = gnome_vfs_expand_initial_tilde (raw);

	g_free (raw);
	g_object_unref (client);

	/* FIXME: handle GConf error */
	return ret;
}

GSList* rcs_gconf_directories ()
{
	GConfClient *client;
	GSList *list, *newdirs, *temp;
	gchar *tmp;

#ifdef RCS_DEBUG
	g_debug ("rcs_gconf_directories\n");
#endif

	client = gconf_client_get_default ();
	
	/* FIXME: handle GConf error */
	list = gconf_client_get_list (client, RCS_GCONF_DIRECTORIES, 
				      GCONF_VALUE_STRING, NULL);

	newdirs = gconf_client_get_list (client, RCS_GCONF_NEW_DIRECTORIES,
					 GCONF_VALUE_STRING, NULL);

	for ( temp = newdirs; temp; temp = g_slist_next (temp))
		list = g_slist_prepend (list, temp->data);

	g_slist_free (newdirs);

#ifdef RCS_DEBUG
	g_debug ("finished converting directories\n");
#endif

	return list;
}

RevisionedObject* rcs_robj_new ()
{
	RevisionedObject *robj;

	robj = (RevisionedObject*) g_malloc (sizeof (RevisionedObject));
	
	robj->uri = NULL;
	robj->revision = NULL;
	robj->file_info = NULL;

	return robj;
}

void rcs_robj_free (RevisionedObject *robj)
{
	if (robj->uri) gnome_vfs_uri_unref (robj->uri);
	if (robj->revision) rcs_revision_free (robj->revision);
	if (robj->file_info) gnome_vfs_file_info_unref (robj->file_info);

	/*	g_free (robj); */
}

Revision* rcs_revision_new ()
{
	Revision *revision;

	revision = (Revision*) g_malloc (sizeof (Revision));

	revision->named = false;
	revision->number = 0;
	revision->name = NULL;
	revision->date = NULL;

	return revision;
}

gchar* 
rcs_get_revision_shortname_for_display (Revision *rev)
{
	GDate *date;
	GString *str;
	double temp;
	gchar *end;

	if (rev->named)
		return g_strdup (rev->name);

	date = g_date_new ();
	
	temp = (rev->date[0] - '0') * 1000 +
		(rev->date[1] - '0') * 100 +
		(rev->date[2] - '0') * 10 +
		(rev->date[3] - '0') * 1;
	g_date_set_year (date, temp);
	
	temp = (rev->date[5] - '0') * 10 +
		(rev->date[6] - '0') * 1;
	g_date_set_month (date, temp);
	
	temp = (rev->date[8] - '0') * 10 +
		(rev->date[9] - '0') * 1;
	g_date_set_day (date, temp);

	if (!g_date_valid (date)) 
		{
			g_debug ("couldn't parse date, returning raw");
			
			return g_strdup (rev->date);
		}
	
	str = g_string_new ("");
	g_string_append_printf (str, "%i/%i/%i", g_date_get_month (date),
				g_date_get_day (date), g_date_get_year (date));
	/* FIXME: i18n issues */

	return g_string_free (str, FALSE);
}

GnomeVFSURI*
rcs_revision_get_uri (GnomeVFSURI *loc, Revision *rev)
{
	GnomeVFSURI *ret;
	GString *str;
	gchar *scheme;

	g_debug ("rev->date: %s; rev->number: %d", rev->date, rev->number);
	
	str = g_string_new ("svn://");
	g_string_append_printf (str, "%d:%s", rev->number, gnome_vfs_uri_get_path (loc));

	scheme = g_string_free (str, FALSE);
	ret = gnome_vfs_uri_new (scheme);

	g_debug ("uri: %s => %s", scheme, gnome_vfs_uri_get_path (ret));

	return ret;
}

void rcs_revision_free (Revision *revision)
{
	if (revision->date) g_free (revision->date);

	/*	g_free (revision); */
}
