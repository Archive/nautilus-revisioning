/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* nautilus-nrevision.c: implementation of the guts of the extension */

#include <config.h>
#include <string.h>
#include <libnautilus-extension/nautilus-extension-types.h>
#include <libnautilus-extension/nautilus-file-info.h>
#include <libnautilus-extension/nautilus-menu-provider.h>
#include "nautilus-nrevision.h"
#include "svn-import.h"

static GObjectClass *parent_class;

static void
import_folder_cb (NautilusMenuItem *item,
		  gpointer user_data)
{
	NautilusFileInfo *file;
	gchar *name, *uri;
	GnomeVFSURI *path;
	
	file = NAUTILUS_FILE_INFO (((GList*)g_object_get_data (item, "file"))->data);
	name = nautilus_file_info_get_name (file);
	g_debug ("importing %s from nautilus\n", name);
	
	uri = nautilus_file_info_get_uri (file);
	path = gnome_vfs_uri_new (uri);

	rcs_init ();
	svn_init ();

	nr_import (gnome_vfs_uri_get_path (path), NULL, NULL);

	svn_shutdown ();
	
	gnome_vfs_uri_unref (path);
	g_free (uri);
	g_free (name);
}

static void
remove_folder_cb (NautilusMenuItem *item,
		  gpointer user_data)
{
	NautilusFileInfo *file;
	gchar *name, *uri;
	GnomeVFSURI *path;
	
	file = NAUTILUS_FILE_INFO (((GList*)g_object_get_data (item, "file"))->data);
	name = nautilus_file_info_get_name (file);
	g_debug ("removing %s from nautilus\n", name);
	
	uri = nautilus_file_info_get_uri (file);
	path = gnome_vfs_uri_new (uri);

	rcs_init ();
	svn_init ();

	nr_un_import (gnome_vfs_uri_get_path (path));

	svn_shutdown ();
	
	gnome_vfs_uri_unref (path);
	g_free (uri);
	g_free (name);
}

static GList *
nautilus_nr_get_file_items (NautilusMenuProvider *provider,
			    GtkWidget            *window,
			    GList                *files)
{						
	GList *items;
	NautilusFileInfo *dir;
	gboolean local;
	gboolean managed_dir;
	gchar *scheme, *uri;
	GnomeVFSURI *path;
	
	g_debug ("nautilus_nr_get_file_items");

	/* can only deal with one directory at a time */
	if ((files == NULL) || 
	    ((files != NULL) && (files->next != NULL)))
		return NULL;

	g_debug ("file name: %s", nautilus_file_info_get_name (files->data));
	
	/* check to make sure we're dealing with file:// uris */
	dir = files->data;
	scheme = nautilus_file_info_get_uri_scheme (dir);
	local = strncmp (scheme, "file", 4) == 0;
	g_free (scheme);

	if (!local)
		return NULL;

	/* check if it's a folder */
	if (!nautilus_file_info_is_directory (dir))
		return NULL;

	uri = nautilus_file_info_get_uri (dir);
	path = gnome_vfs_uri_new (uri);
	/* check if the folder is managed */
	managed_dir = nr_import_exists (gnome_vfs_uri_get_path (path));
	gnome_vfs_uri_unref (path);
	g_free (uri);

	/* create the menu item */
	if (!managed_dir)
		{
			NautilusMenuItem *item;
			item = nautilus_menu_item_new ("NautilusFR::import_folder",
						       "Monitor History",
						       "Monitor the history of the selected directory",
						       NULL);
			g_signal_connect (item, "activate", G_CALLBACK (import_folder_cb), provider);
			g_object_set_data_full (G_OBJECT (item), "file",
						nautilus_file_info_list_copy (files),
						(GDestroyNotify) nautilus_file_info_list_free);

			items = g_list_append (NULL, item);
		}
	else
		{
			NautilusMenuItem *item;
			item = nautilus_menu_item_new ("NautilusFR::remove_folder",
						       "Stop Monitoring History",
						       "Stop monitoring the history of the selected directory",
						       NULL);
			g_signal_connect (item, "activate", G_CALLBACK (remove_folder_cb), provider);
			g_object_set_data_full (G_OBJECT (item), "file",
						nautilus_file_info_list_copy (files),
						(GDestroyNotify) nautilus_file_info_list_free);

			items = g_list_append (NULL, item);
		}
			

	return items;
}


static void 
nautilus_nr_menu_provider_iface_init (NautilusMenuProviderIface *iface)
{
	iface->get_file_items = nautilus_nr_get_file_items;
}


static void 
nautilus_nr_instance_init (NautilusNr *nr)
{
}


static void
nautilus_nr_class_init (NautilusNrClass *class)
{
	parent_class = g_type_class_peek_parent (class);
}


static GType nr_type = 0;


GType
nautilus_nr_get_type (void) 
{
	return nr_type;
}


void
nautilus_nr_register_type (GTypeModule *module)
{
	static const GTypeInfo info = {
		sizeof (NautilusNrClass),
		(GBaseInitFunc) NULL,
		(GBaseFinalizeFunc) NULL,
		(GClassInitFunc) nautilus_nr_class_init,
		NULL, 
		NULL,
		sizeof (NautilusNr),
		0,
		(GInstanceInitFunc) nautilus_nr_instance_init,
	};

	static const GInterfaceInfo menu_provider_iface_info = {
		(GInterfaceInitFunc) nautilus_nr_menu_provider_iface_init,
		NULL,
		NULL
	};

	nr_type = g_type_module_register_type (module,
					       G_TYPE_OBJECT,
					       "NautilusRevisioning",
					       &info, 0);

	g_type_module_add_interface (module,
				     nr_type,
				     NAUTILUS_TYPE_MENU_PROVIDER,
				     &menu_provider_iface_info);
}
