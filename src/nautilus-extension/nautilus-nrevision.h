/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* nautilus-nrevision.h: header for nautilus extension */

#ifndef NAUTILUS_NREVISION_H
#define NAUTILUS_NREVISION_H

#include <glib-object.h>

G_BEGIN_DECLS

#define NAUTILUS_TYPE_NR  (nautilus_nr_get_type ())
#define NAUTILUS_NR(o)    (G_TYPE_CHECK_INSTANCE_CAST ((o), NAUTILUS_TYPE_NR, NautilusNr))
#define NAUTILUS_IS_NR(o) (G_TYPE_CHECK_INSTANCE_TYPE ((o), NAUTILUS_TYPE_NR))

typedef struct _NautilusNr      NautilusNr;
typedef struct _NautilusNrClass NautilusNrClass;

struct _NautilusNr {
	GObject __parent;
};

struct _NautilusNrClass {
	GObjectClass __parent;
};

GType nautilus_nr_get_type      (void);
void  nautilus_nr_register_type (GTypeModule *module);

G_END_DECLS

#endif /* NAUTILUS_NREVISION_H */
