/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
 *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* shadow.c: Shadow a directory (either with hard links or direct 
   copying) a directory from one location to another */

#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "shadow.h"

typedef struct 
{
	gchar *src_uri;
	gchar *dest_uri;
	GList *reloc;
	GHashTable *resolve;
	GnomeVFSURI *rel_path;
	GnomeVFSURI *trans_path;
} ShadowOptions;

int print_error (GnomeVFSResult code, const gchar *uri)
{
	const gchar *error_desc;

	error_desc = gnome_vfs_result_to_string (code);
	g_printerr ("error %d when accessing %s: %s\n", code, uri, error_desc);

	return code;
}

void print_link_error (int error, const gchar *uri)
{
	g_warning ("link error for \"%s\": %s", uri, strerror (error));
}

gint g_list_str_equal (gpointer data1, gpointer data2)
{
	if (g_str_equal (data1, data2)) return 0;
	return 1;
}

gboolean shadow_indiv_dir (const gchar *rel_path,
			   GnomeVFSFileInfo *info,
			   gboolean recursing_will_loop,
			   gpointer data,
			   gboolean *recurse)
{
	ShadowOptions *options = (ShadowOptions*) data;
	const gchar *path;
	GnomeVFSURI *rel, *rel_parent, *temp;
	GString *transformed_name = g_string_new (info->name);
  
	g_printf ("===================================================\n");
	g_printf ("Current src path: %s\nCurrent dest path: %s\n",
		  gnome_vfs_uri_to_string (options->rel_path, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD), 
		  gnome_vfs_uri_to_string (options->trans_path, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD));

	if (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_TYPE != GNOME_VFS_FILE_INFO_FIELDS_TYPE)
		g_error ("Couldn't get file info for %s!\n", rel_path);

	/* Make sure transformed path is in sync with current path */

	g_printf ("rel path: %s\n", rel_path);

	rel = gnome_vfs_uri_new (options->src_uri);
	rel = gnome_vfs_uri_append_path (rel, rel_path);

	g_printf ("path: %s\n", gnome_vfs_uri_to_string (rel, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD));
  
	/* figure out the correct path preceding the current item */
	rel_parent = gnome_vfs_uri_new (gnome_vfs_uri_extract_dirname (rel));
  
	g_printf ("rel path parent: %s\n", 
		  gnome_vfs_uri_to_string (rel_parent, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD));

	/* in the same directory - replace relative path with transformed path*/
	while (!gnome_vfs_uri_is_parent (rel, options->rel_path, FALSE) && 
	       !gnome_vfs_uri_is_parent (options->rel_path, rel, FALSE))
		{
			GnomeVFSURI *temp;
      
			g_printf ("\"%s\" != \"%s\"\n",
				  gnome_vfs_uri_to_string (rel_parent, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD),
				  gnome_vfs_uri_to_string (options->rel_path, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD)); 
			g_printf ("descending from %s\n", 
				  gnome_vfs_uri_to_string (options->rel_path, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD));

      
			/*      g_printf ("pop'ing bottom-level from source-path \"%s\"\n", 
				gnome_vfs_uri_to_string (options->rel_path, GNOME_VFS_URI_HIDE_NONE)); */
			temp = gnome_vfs_uri_get_parent (options->rel_path);
			gnome_vfs_uri_unref (options->rel_path);
			options->rel_path = temp;
      
			/*      g_printf ("pop'ing bottom-level from transformed-path \"%s\"\n", 
				gnome_vfs_uri_to_string (options->trans_path, GNOME_VFS_URI_HIDE_NONE)); */
			temp = gnome_vfs_uri_get_parent (options->trans_path);
			gnome_vfs_uri_unref (options->trans_path);
			options->trans_path = temp;
		}
  
	gnome_vfs_uri_unref (rel_parent);
  
	/* Change path of the item */

	temp = gnome_vfs_uri_dup (options->trans_path);
	temp = gnome_vfs_uri_append_string (temp, info->name);
	path = gnome_vfs_uri_to_string (temp, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD);
	gnome_vfs_uri_unref (temp);

	/* Rename the item if necessary */
	if (g_list_find_custom (options->reloc, info->name, (GCompareFunc*)g_list_str_equal))
		{
			GnomeVFSURI *uri = gnome_vfs_uri_dup (options->trans_path);
			GString *path_str = g_string_new (path);
			GString *new_name = g_string_new (rel_path);

			g_printf ("test: %s\n", rel_path);

			g_string_append_c (path_str, '.');
			g_string_append_c (new_name, '.');

			/* we have to find a unique filename to use for the relocation */
			do 
				{
					gnome_vfs_uri_unref (uri);
					g_string_append (path_str, "1");
					g_string_append (new_name, "1");
					uri = gnome_vfs_uri_dup (options->trans_path);
					uri = gnome_vfs_uri_append_file_name (uri, path_str->str);
				} while (gnome_vfs_uri_exists (uri));

			g_hash_table_insert (options->resolve, g_strdup (new_name->str), g_strdup (rel_path));
			path = g_strdup (path_str->str);
      
			g_string_free (path_str, TRUE);
			g_string_free (new_name, TRUE);
			gnome_vfs_uri_unref (uri);

			g_printf ("relocating %s to %s\n", info->name, path);
		}

	g_printf ("path: %s\n", path);

	/* Check if it's a file */
	if (info->type == GNOME_VFS_FILE_TYPE_REGULAR)
		{
			GnomeVFSURI *dest_uri = gnome_vfs_uri_new (path);
			GnomeVFSURI *src_uri = gnome_vfs_uri_new (options->src_uri);

			src_uri = gnome_vfs_uri_append_string (src_uri, rel_path);

			g_printf ("linking \"%s\" to \"%s\"\n", 
				  gnome_vfs_uri_to_string (src_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD), 
				  gnome_vfs_uri_to_string(dest_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD)); 
      
			/* FIXME: gnomevfs doesn't support hard links, but this is very, very useful */
			if (link (gnome_vfs_uri_to_string (src_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD), 
				  gnome_vfs_uri_to_string (dest_uri, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD)) == -1)
				print_link_error (errno, rel_path);

			gnome_vfs_uri_unref (dest_uri);
			gnome_vfs_uri_unref (src_uri);
		}

	/* Check if it's a directory */
	else if (info->type == GNOME_VFS_FILE_TYPE_DIRECTORY)
		{
			g_printf ("creating new directory \"%s\"\n", path);
			if (info->valid_fields & GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS == GNOME_VFS_FILE_INFO_FIELDS_PERMISSIONS)
				gnome_vfs_make_directory (path, info->permissions);

			gnome_vfs_uri_unref (options->trans_path);

			options->rel_path = gnome_vfs_uri_append_string (options->rel_path, info->name);
			options->trans_path = gnome_vfs_uri_new (path);
			g_printf ("ascending into %s\n", gnome_vfs_uri_to_string (options->rel_path, GNOME_VFS_URI_HIDE_TOPLEVEL_METHOD));

			*recurse = !recursing_will_loop;
		}

	g_string_free (transformed_name, TRUE);

	return TRUE;
}

GnomeVFSResult shadow_dir (gchar *src_uri, gchar *dest_uri, GList *reloc, GHashTable *resolve)
{
	ShadowOptions *options;

	options = (ShadowOptions*) g_malloc (sizeof (ShadowOptions));
	options->src_uri = src_uri;
	options->dest_uri = dest_uri;
	options->reloc = reloc;
	options->resolve = resolve;
	options->rel_path = gnome_vfs_uri_new (src_uri);
	options->trans_path = gnome_vfs_uri_new (dest_uri);

	return gnome_vfs_directory_visit (src_uri, GNOME_VFS_FILE_INFO_DEFAULT, 
					  GNOME_VFS_DIRECTORY_VISIT_LOOPCHECK,
					  (GnomeVFSDirectoryVisitFunc) shadow_indiv_dir,
					  options);
}
