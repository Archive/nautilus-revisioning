/* -*- Mode: C; tab-width: 8; indent-tabs-mode: 8; c-basic-offset: 8 -*- */

/*
 * Nautilus-Revisioning
 *
 * Copyright (C) 2005, Matt Jones
  *
 * Nautilus is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * Nautilus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors: Matt Jones <mattjones@berkeley.edu
 *
 */

/* rcs-utils.h: general-purpose data structures and functions
   used by every rcs implementation */

#ifndef _RCS_UTILS_H_
#define _RCS_UTILS_H_

#include <glib.h>
#include <libgnomevfs/gnome-vfs.h>
#include "shadow.h"

#define RCS_GCONF_REPOSITORY "/apps/nautilus-revisioning/repository"
#define RCS_GCONF_DIRECTORIES "/apps/nautilus-revisioning/directories"
#define RCS_GCONF_NEW_DIRECTORIES "/apps/nautilus-revisioning/new-directories"
#define RCS_GCONF_RM_DIRECTORIES "/apps/nautilus-revisioning/rm-directories"
#define RCS_GCONF_DIRECTORY "/apps/nautilus-revisioning"


/*#define RCS_DEBUG*/

typedef enum {false=0, true} bool;

typedef struct 
{
	bool named;
	glong number;
	const gchar *date;
	const gchar *name;
} Revision;

typedef struct
{
	GnomeVFSURI *uri;
	Revision *revision;
	GnomeVFSFileInfo *file_info;
	/* Various metadata stuff should go here */
} RevisionedObject;

typedef struct {
	GnomeVFSURI *uri;
	const gchar *tag_name;
} RCSFileControlData;

typedef enum {NR_UNKNOWN=0, NR_ADDED, NR_DELTA} NRAction;

typedef void(* nr_notify_cb)(const gchar *path, NRAction action, void *baton);

GnomeVFSResult nautilus_rcs_do_tag (RCSFileControlData *data);

gchar* rcs_gconf_repository ();
GSList* rcs_gconf_directories ();

RevisionedObject* rcs_robj_new ();
Revision* rcs_revision_new ();

gchar* rcs_get_revision_shortname_for_display (Revision *rev);
GnomeVFSURI* rcs_revision_get_uri (GnomeVFSURI *loc, Revision *rev);

void rcs_robj_free (RevisionedObject *robj);
void rcs_revision_free (Revision *revision);

#endif /* _RCS_UTILS_H_ */

